# SSH Keys

**NEVER COMMIT/PUSH PRIVATE KEYS**

This structure is needed to keep multiple ssh keys, of a lot of servers of some identities with just one config file.
The trick behind this is keep in all machines where this repository is cloned a symbolic link to the key file.


## Naming of files

- Physical files:

> id\_<protocol>-<server>-<machine_name>-<git_server>-<identity>

For a public key with rsa protocol, personal (pessoal) bitbucket public key of *moningrado* machine, the public file will became:

> id\_rsa-moningrado-bitbucket-pessoal.pub

And the private key:

> id\_rsa-moningrado-bitbucket-pessoal


## Links

Links are needed to keep just one ssh config file


## Ignored files

- All private keys, of course
- Links to public keys, mentioned in `config` file


## Pratical effects

- On a given machine will be just the public key files of another machines
- The `config` file will work on every machine

