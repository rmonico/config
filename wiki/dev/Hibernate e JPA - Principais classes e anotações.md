# JPA (Java Persistence API)

Conferir todas as informações neste arquivo, foi criado de cabeça!  
Repositório: <http://mvnrepository.com/artifact/org.hibernate/hibernate-core>  

## Principais anotações  

- **`@Entity`**: Obrigatório para maepar uma classe como uma entidade.
- **`@Table`**: Usado para acrescentar informações ao mapeamento da tabela.
- **`@Id`**: Indica que o campo faz parte da chave primária.
- **`@GeneratedValue`**: Indica que o valor do campo é auto-gerado.
- **`@Column`**: Indica que o campo corresponde a uma coluna do banco de dados *(opcional, usado para acrescentar informações ao mapeamento)*
- **`@Temporal(TemporalType.DATE | TIME | TIMESTAMP)`**: Indica qual(s) parte(s) de campo `Calendar` deve ser persistido no banco de dados.

## Principais classes

- **`EntityManagerFactory`**: Factory do `EntityManager`
- **`EntityManager`**: Gerencia as entidades (roda as queries), devolve a session, a transaction, etc **(Não sei se faz parte do Hibernate ou da JPA)**  
- **`Session`**: Representa uma sessão com o banco de dados **(A classe não é essa! Pegar o nome correto!)**  
- **`EntityTransaction`**: Responsável pelo controle de transação  