# Maven

## Novo projeto

    artifactId=produtos
    groupId=br.com.alura.maven
    mvn archetype:generate -DartifactId=$artifactId -DgroupId=$groupId -DinteractiveMode=false -DarchetypeArtifactId=maven-archetype-quickstart

- **archetype:generate**: Gera um novo projeto do maven  
- **artifactId**: ID do novo artefato  
- **groupId**: Grupo do novo artefato
- **archetypeArtifactId**: Archetype no qual o novo projeto é baseado.  

(*-D,--define <arg>: Define a system property*)  

Principais Archetypes:

- **maven-archetype-j2ee-simple**: An archetype which contains a simplifed sample J2EE application  
- **maven-archetype-quickstart**: An archetype which contains a sample Maven project  
- **maven-archetype-site**: An archetype which contains a sample Maven site  
- **maven-archetype-webapp**: An archetype which contains a sample Maven Webapp project  


Também é possível criar um novo projeto do maven no Eclipse pelo menu `File/New project`, escolher o wizard `Maven project` e entrar com as mesmas informações da linha de comando.  
No teste que eu fiz enquanto estava fazendo este curso na Alura gerou um pom igual para as duas opções (por linha de comando e pelo Eclipse), só mudou a versão do projeto.  

## Projeto do eclipse

- **eclipse:eclipse**: Gera o projeto do eclipse  
- **eclipse:clean**: Regera o projeto do eclipse  

## Relatório de Testes

- **surefire-report:report**: Gera um relatório com o plugin 'surfire report' em HTML em '/target/surefire-reports/TEST-$groupId.AppTest.xml'  
- **package**: Empacota de acordo com a tag '<packaging\>' do pom.xml  

## Adicionar nova dependência

[Repositório do Maven](http://mvnrepository.com/)  
[Maven Central](https://search.maven.org/)  
Mexer no pom.xml, tag '<dependencies>' (não custa falar)  
**Importante**: notar que o repositório lista apenas os pacotes, não os plugins! Para os plugins do maven é necessário procurar na internet!  

## Conceitos principais

### Ciclo de vida

O maven possui três ciclos de vida pré-definidos: default, clean e site.

<https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html>

Fases do ciclo de vida default:  

- **validate**: validate the project is correct and all necessary information is available  
- **compile**: compile the source code of the project  
- **test**: test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed  
- **package**: take the compiled code and package it in its distributable format, such as a JAR.  
- **verify**: run any checks on results of integration tests to ensure quality criteria are met  
- **install**: install the package into the local repository, for use as a dependency in other projects locally  
- **deploy**: done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.  
- **clean**: Limpa a pasta 'target'  

### Archetypes

Archetypes são modelos de projetos (eu acho), ver os principais archetypes do maven na seção principal

### Escopos

Cada dependência deve ter um escopo. Os escopos possíveis são:

- **compile**: Disponível para compilação e em tempo de execução.  
- **provided**: Disponível para compilação, mas não é empacotado no artefato resultante (jar ou war). Usado para dependências fornecidas pelo ambiente mas necessárias para geração das classes.
- **runtime**: Disponível apenas em tempo de execução mas não para geração das classes.  
- **test**: Disponível apenas para os testes.  

## Plugins

### Plugin `versions`

Tarefas relacionadas à versão das dependências [docs](http://www.mojohaus.org/versions-maven-plugin/)  

- **versions:use-latest-versions**: atualiza o pom para usar as últimas versões das dependências  
- **versions:display-dependency-updates**: Mostra quais dependências podem ser atualizadas  

### Plugin PMD

Analisador de código-fonte

Basta acresentar as seguintes linhas ao pom.xml:

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.6</version>
                <executions>
                    <execution>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

Os seguintes goals são acrescentados ao projeto:

- **pmd:pmd**: creates a PMD site report based on the rulesets and configuration set in the plugin. It can also generate a pmd output file aside from the site report in any of the following formats: xml, csv or txt.  
- **pmd:cpd**: generates a report for PMD's Copy/Paste Detector (CPD) tool. It can also generate a cpd results file in any of these formats: xml, csv or txt.  
- **pmd:check**: verifies that the PMD report is empty and fails the build if it is not. This goal is executed by default when pmd:pmd is executed.  
- **pmd:cpd-check**: verifies that the CPD report is empty and fails the build if it is not. This goal is executed by default when pmd:cpd is executed.  

Quando alguma checagem do PMD é violada um relatório de erros é gerado na pasta 'target/site', é necessário chamar manualmente os goals do pmd com `mvn pmd:pmd`, `mvn pmd:check`, etc  
Porém, com a configuração acima o pmd:check é rodado automaticamente pela fase `verify` do ciclo de vida `default`  

[Mais informações do PMD](https://maven.apache.org/plugins/maven-pmd-plugin/usage.html)  

### JaCoCo

Gera relatórios de cobertura de testes em: 'target/site/jacoco/index.html'

    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.7.9</version>
        <executions>
            <execution>
                <goals>
                    <goal>prepare-agent</goal>
                    <goal>report</goal>
                </goals>
            </execution>
        </executions>
    </plugin>

[Site do projeto](http://www.eclemma.org/jacoco/trunk/doc/maven.html)

### Jetty

Roda um servidor do Jetty com as configurações definidas no pom.xml  

<http://www.eclipse.org/jetty/documentation/current/jetty-maven-plugin.html#jetty-run-goal>  
<http://mvnrepository.com/artifact/org.eclipse.jetty/jetty-maven-plugin>  

    <plugin>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-maven-plugin</artifactId>
        <version>9.4.3.v20170317</version>
        <configuration>
            <scanIntervalSeconds>10</scanIntervalSeconds>
            <webApp>
                <contextPath>/test</contextPath>
            </webApp>
        </configuration>
    </plugin>

Depois rodar `mvn jetty:run` e ver o servidor subir na URL <http://localhost:8080/test/>


### Help

<http://maven.apache.org/plugins/maven-help-plugin/index.html>  

- `mvn help:describe -DartifactId=project_artifact -DgroupId=project_group`  
    Mostra todos os goals para o projeto  


## Exclusão de dependências

    <exclusions>
        <exclusion>
            <groupId>xmlpull</groupId>
            <artifactId>xmlpull</artifactId>
        </exclusion>            
    </exclusions>
