# Tomcat

## Contextos

São definidos no arquivo `server.xml` do servidor através da tag `<Context>` na forma:  

```
<Context docBase="jsf-livraria" path="/jsf-livraria" reloadable="true" source="org.eclipse.jst.jee.server:jsf-livraria"/></Host>
```

**Observação:** Normalmente há ferramentas gráficas razoáveis para realizar esta tarefa e atualizar o servidor após mexer no arquivo, portanto não se costuma mexer nesse arquivo na mão.  