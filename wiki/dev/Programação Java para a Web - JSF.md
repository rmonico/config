# JSF

O JSF faz o papel de controller e fornece tags para trabalhar com o front-end. O JSF em sí é apenas uma especificação e é disponibilizado na forma de um Servlet, por esse motivo depende de um servidor WEB para funcionar.

Implementação  | Site
:-             | :-
Sun Mojarra    | <http://javaserverfaces.java.net/>
Apache MyFaces | <http://myfaces.apache.org/>

Também é possível estender o JSF com novos componentes. Os mais populares são:  

Biblioteca         | Site
:-                 | :-
MyFaces - Tomahawk | <http://myfaces.apache.org/tomahawk/>  
MyFaces - Trinidad | <http://myfaces.apache.org/trinidad/>  
MyFaces - Tobago   | <http://myfaces.apache.org/tobago/>  
JBoss - RichFaces  | <http://www.jboss.org/richfaces/>  
ICESoft - ICEFaces | <http://www.icefaces.org/>  
Open? - PrimeFaces | <http://www.primefaces.org/>  

## Dúvidas

- No web.xml qual a diferença para os context-param facelets.DEVELOPMENT e javax.faces.PROJECT_STAGE ?  

## Taglib URI's

```
xmlns:h="http://java.sun.com/jsf/html"
xmlns:f="http://java.sun.com/jsf/core"
```

## Configuração

1. Adicionar configuração do Servlet do JSF ao `web.xml`:

```
<!-- Faces Servlet -->  
<servlet>  
    <servlet-name>Faces Servlet</servlet-name>  
    <servlet-class>javax.faces.webapp.FacesServlet</servlet-class>  
    <load-on-startup>1</load-on-startup> <!-- Ver na wiki: <Servlets.md> -->  
</servlet>  

<!-- Faces Servlet Mapping -->  
<servlet-mapping>  
    <servlet-name>Faces Servlet</servlet-name>  
    <url-pattern>*.jsf</url-pattern>  
</servlet-mapping>  

<!-- Não sei o que é... -->  
<context-param>  
    <param-name>javax.faces.STATE_SAVING_METHOD</param-name>  
    <param-value>server</param-value>  
</context-param>  

<!-- Use Documents Saved as *.xhtml -->  
<context-param>  
    <param-name>javax.faces.DEFAULT_SUFFIX</param-name>  
    <param-value>.xhtml</param-value>  
</context-param>  

<!-- Special Debug Output for Development -->  
<context-param>  
    <param-name>facelets.DEVELOPMENT</param-name>  
    <param-value>true</param-value>  
</context-param>  

<!-- Mais informação de debug -->  
<context-param>  
    <param-name>javax.faces.PROJECT_STAGE</param-name>  
    <param-value>Development</param-value>  
</context-param>  

<!-- Também não sei o que é, acho que é alguma coisa do Primefaces -->  
<servlet>  
    <servlet-name>Resource Servlet</servlet-name>  
    <servlet-class>org.primefaces.resource.ResourceServlet</servlet-class>  
    <load-on-startup>1</load-on-startup>  
</servlet>  

<!-- Também não sei o que é, acho que é alguma coisa do Primefaces -->  
<servlet-mapping>  
    <servlet-name>Resource Servlet</servlet-name>  
    <url-pattern>/primefaces_resource/*</url-pattern>  
</servlet-mapping>  
```

## Classe `FacesContext`


> JSF defines the  javax.faces.context.FacesContext abstract base class for representing all of the contextual information associated with processing an incoming request, and creating the corresponding response.  
> A FacesContext instance is created by the JSF implementation, prior to beginning the request processing lifecycle, by a call to the getFacesContext method of  FacesContextFactory, as described in Section 6.6 “FacesContextFactory”.  
> When the request processing lifecycle has been completed, the JSF implementation will call the release method, which gives JSF implementations the opportunity to release any acquired resources, as well as to pool and recycle FacesContext instances rather than creating new ones for each request.  

[Fonte: especificação JSF 2.0](https://jcp.org/aboutJava/communityprocess/final/jsr314/index.html)

## Mapeamento de beans

No `faces-config.xml`

```
<managed-bean>
    <managed-bean-name>nome_usado_para_referenciar_o_bean_no_xhtml</managed-bean-name>
    <managed-bean-class>full.qualified.class.Name</managed-bean-class>
    <managed-bean-scope>request</managed-bean-scope>
</managed-bean>
```

Também é possível definir um bean através de anotação. Nesse caso escopo depende da anotação usada e o nome do bean (caso não seja informado via propriedade `name`) será o nome da classe começando com minúscula.  

managed-bean-scope | Anotação correspondente | Tempo de vida
:-                 | :-                      | :-
**application**    | `@ApplicationScoped`    | Enquanto o container estiver no ar  
**session**        | `@SessionScoped`        | Durante a sessão de um usuário  
**request**        | `@RequestScoped`        | Durante o request  
**none**           | `@NoneScoped`           | Uma nova instância é criada para cada vez que o código de referir ao bean (?)  
**view**           | `@ViewScoped`           | Durante a renderização do view  
**custom**         | `@CustomScoped`         | A instância do bean é salva em um map (que é o valor dessa configuração). Nesse caso o managed-bean-name é a chave no mapa referenciado pelo scope

### managed-property's

```
<managed-bean>
    <managed-bean-name>nome_usado_para_referenciar_o_bean_no_xhtml</managed-bean-name>
    <managed-bean-class>full.qualified.class.Name</managed-bean-class>
    <managed-bean-scope>request</managed-bean-scope>
    <managed-property>
        <property-name>categoriaBean</property-name>
        <value>#{categoriaBean}</value>
    </managed-property>
    <managed-property>
        <property-name>contextoBean</property-name>
        <value>O valor também pode ser uma constante</value>
    </managed-property>
    <managed-property>
        <property-name>um_field_so_pra_inicializar_com_null</property-name>
        <null-value/>
    </managed-property>
    <managed-property>
        <property-name>um_field_do_tipo_hashmap</property-name>
        <map-entries>
            <map-entry><key>A</key><value>Valor da chave A</value></map-entry>
            <map-entry><key>B</key><value>Valor da chave B</value></map-entry>
        </map-entries>
    </managed-property>
    <managed-property>
        <property-name>um_field_do_tipo_ArrayList</property-name>
        <list-entries>
            <value>Item 1 da lista</value>
            <value>Item 2 da lista</value>
        </list-entries>
    </managed-property>    
</managed-bean>
```

Também é possível definir uma managed-property através da anotação @ManagedProperty. Nesse caso é necessário gerar os métodos `get` e `set`.

## Tags do `http://java.sun.com/jsf/html`

Tag | Funcionalidade  
:- | :-  
`<h:message>` | Define onde irão ficar a área de mensagens da página  
