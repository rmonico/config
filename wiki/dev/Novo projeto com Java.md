# Criar novo projeto

## Java

- Criar pasta para o projeto (com o nome do mesmo)
- Gerar .gitignore do gitignore.io
  - Com as tecnologias (no mínimo): Java, Eclipse, Netbeans, Gradle, 
- Fazer o commit inicial


## Gradle

- Rodar `gradle init` na pasta do projeto  
  - Se o Gradle não estiver instalado é possível baixá-lo em `http://www.gradle.org/downloads`
  - É recomendado usar um gestor de pacotes ao invés de fazer essa instalação manualmente: `http://sdkman.io/sdks.html` ou `https://chocolatey.org/`
- Criar o wrapper para o gradle (opcional): `gradle wrapper --gradle-version 3.4.1`
- Adequar as configurações no build.gradle (opcional)
- Rodar `./gradlew build` para testar o projeto criado
- Ver o que é possível fazer com o Gradle (opcional): `./gradlew tasks`

### Macetes

- Os fontes ficam na pasta `src/main/java`, caso o projeto tenha fontes existentes os mesmos devem ser colocados lá


## Gerando o projeto do Eclipse

- Habilitar o plugin do eclipse no `build.gradle`: `apply plugin: 'eclipse'`
- **Com o Gradle instalado e funcionando** criar o projeto com `gradle eclipse`.
- Em caso de problema com o projeto no eclipse é possível recriá-lo com `gradle cleanEclipse`
