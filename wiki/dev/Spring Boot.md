# Spring Boot

**Criando a aplicação:**  

`mvn archetype:generate -DgroupId=com.alura.listavip -DartifactId=com.alura.listavip -DinteractiveMode=off -DarchetypeArtifactId=maven-archetype-quickstart`

**Adicionar às dependências:**  

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
      <version>1.4.1.RELEASE</version>
    </dependency>


**Classe Main:**

```
package com.alura.listavip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@SpringBootApplication // Indica que a classe é uma aplicação do Spring boot
@Controller // Indica que a classe será um controller
public class Configuracao {

    @RequestMapping("/") // Mapeia a URL / para este método
    @ResponseBody // TODO Não sei
    public String ola() {
        return "Ola, Bem bindo ao sistema Lista VIPs"; // "HTML" retornado
    }

    public static void main(String[] args) {
        SpringApplication.run(Configuracao.class, args);
    }

}
```

Rodar como classe main do Java (sem segredo)

# Um mapeamento simples

```
package com.alura.listavip;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ConvidadoController {

    @RequestMapping("/")
    public String index() {
        return "index";
    }

}
```

Criar um `index.html` na pasta `/com.alura.listavip/src/main/resources/templates` (que deverá ser criada manualmente) com o conteúdo:

```
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ListaVIP</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<div class="jumbotron" align="center" style="margin-top: 50px;">
			<h1>Seja bem-vindo ao ListaVIPs</h1>
			<div align="center">
				<a href="listavip" class="btn btn-lg btn-primary">Clique aqui
					para ver a lista de convidados</a>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
```

Baixar (manualmente) o bootstrap de <http://getbootstrap.com/getting-started/#download> e descompactar em `/com.alura.listavip/src/main/resources/static` (que também deverá ser criada manualmente)  


## Spring Data

**Dependência no pom.xml:**

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
    <version>1.4.2.RELEASE</version>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.40</version>
</dependency>
```

