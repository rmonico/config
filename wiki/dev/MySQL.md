# MYSQL

**Instalar o pacote do mariadb:**

`pacman -S mariadb`

**Inicializar o cluster do servidor  **


```
mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

```

**TODO:** Criar usuário root com senha padrão


**Habilitar no boot e inicializar o serviço do mariadb**

```
systemctl enable mariadb.service
systemctl start mariadb.service
```

**Conectar no banco de dados com o cliente de linha de comando**

`mysql -u root`