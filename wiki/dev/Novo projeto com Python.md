# Novo projeto com Python

- Criar uma pasta para o projeto:

    nome=nome_do_projeto
    mkdir "$nome"

- Criar as dependências para serem geridas com o pip:

    echo 'dependencia' > $nome/requirements

- Gerar o ambiente com as novas dependências:

    mkvirtualenv "$nome" -a "$nome" -r "requirements"

- Criar o .gitignore:

    git ignore lista,de,tecnologias > .gitignore

- Criar o projeto no SublimeText:
    - Menu: Project/Save Project As...

- Versionar o projeto e fazer o commit inicial

    git init .
    git add .
    git commit -m mensagem