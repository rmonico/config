# Referência de Markdown

Exemplo rápido das principais sintaxes do Markdown

## Tabelas

```
Esquerda | Centralizado | Direita
:- | :-: | -:
1,1 | 2,1 | 3,1 
1,2 | 2,2 | 3,2 
1,3 | 2,3 | 3,3 
```

Produz:

Esquerda | Centralizado | Direita
:- | :-: | -:
1,1 | 2,1 | 3,1 
1,2 | 2,2 | 3,2 
1,3 | 2,3 | 3,3 

## Listas

- Lista
- Não
- Numerada
- Outro exemplo de lista não-numerada  
    Mas dessa vez com parágrafo aqui  
que pode ter mais de uma linha, mas a segunda  
linha não precisa estar identada, apenas a  
linha inicial do parágrafo. Outro detalhe:  
Deve ter dois espaços no final de cada uma  
dessas linhas...
- Outro item na mesma lista  

        System.out.println("Um detalhe importante:");
        System.out.println("Para colocar código dentro de um item de lista");
        System.out.println("é necessário identá-lo duas vezes...");

\- Lista  
\- Não  
\- Numerada  

Entre uma lista não-numerada e uma numerada deve haver alguma coisa, se não dá problema:

1. Primeiro item
2. De uma
3. Lista numerada

1\. Primeiro item  
2\. De uma  
3\. Lista numerada  

## Links

[Link](http://link.com)  
    [Link](http://link.com\)

Link automático: <http://google.com/> <http://google.com/\>

[Link com referência, a referência pode estar em qualquer lugar no documento] [referencia]  
[O importante da referência é que ela pode ser reutilizada] [referencia]  
[referencia]: http://referencia.com "Título opcional"  

    [Link com referência, a referência pode estar em qualquer lugar no documento\] [referencia\]
    [O importante da referência é que ela pode ser reutilizada\] [referencia\]
    [referencia]: http://referencia.com "Título opcional"

## Código

Um pouco de código \``system.out.println("Uma linha simples de código");`\` no meio de uma linha normal

    system.out.println("Um bloco de código... Notar que a fonte muda");
    system.out.println("Um bloco de código... Notar que a fonte muda");
    system.out.println("Um bloco de código... Notar que a fonte muda");
    system.out.println("Um bloco de código... Notar que a fonte muda");
    system.out.println("Um bloco de código... Notar que a fonte muda");

(Só identar com quatro espaços\)  

## Texto & formatação

Texto "solto" não precisa de qualquer formatação especial

**\*\*Negrito\*\***  
*\*Itálico\**  

Notar que quando não tem nada entre duas
linhas ele coloca tudo na mesma linha  
Mas se tiver dois ou mais espaços  
no final da linha não 

Terminar uma linha dois ou mais espaços é diferente de deixar uma linha vazia

\*\*\* produz uma linha horizontal:
***

## Títulos
#\# Subtítulo nível 1

##\#\# Subtítulo nível 2

###\#\#\# Subtítulo nível 3

####\#\#\#\# Acho que pode ir até o 6

## Citações

> Citação
> 
> Citação com mais de uma linha precisa ter uma linha vazia entre elas
> 
> > Citação dentro da citação

    > Citação
    > 
    > Citação com mais de uma linha precisa ter uma linha vazia entre elas
    > 
    > > Citação dentro da citação

## Caracteres especiais

Para escapar um caractere especial basta acrescentar uma \`\\\` antes do mesmo:

Caractere | Nome | Escapado
:-: | :-: | :-: 
**\\**   | barra invertida    | \\\\
**\`**   | crase              | \\\`
**\***   | asterisco          | \\\*
**\_**   | sublinhado         | \\\_
**\{\}** | chaves             | \\\{\}
**\[\]** | colchetes          | \\\[\]
**\(\)** | parênteses         | \\\(\)
**\#**   | sustenido          | \\\#
**\+**   | sinal de soma      | \\\+
**\-**   | sinal de subtração | \\\-
**\.**   | ponto              | \\\.
**\!**   | exclamação         | \\\!

## Imagens

\![alt text\](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg/305px-The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg "Título da imagem"\)
![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg/305px-The_Sun_by_the_Atmospheric_Imaging_Assembly_of_NASA%27s_Solar_Dynamics_Observatory_-_20100819.jpg "Título da imagem")

E uma imagem também pode referenciar um path relativo à pasta arquivo markdown atual
