# Criar SSB no Firefox do Windows

- Rodar o firefox com o parâmetro `--ProfileManager`
- Criar um novo perfil e abrir ele
- Ir no menu `Personalizar` e habilitar a barra de título
- Abrir as configurações avançadas do firefox na url `about:config`
- Alterar o valor da configuração `toolkit.legacyUserProfileCustomizations.stylesheets` para `true`
- Abrir o endereço `about:support`
- Encontrar a pasta do perfil e abri-la no explorer
- Criar uma pasta `chrome`
- Dentro desta pasta criar o arquivo `userChrome.css` com o conteúdo abaixo

```css
/*
 * Do not remove the @namespace line -- it's required for correct functioning
 */
@namespace url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"); /* set default namespace to XUL */

/*
 * Hide tab bar, navigation bar and scrollbars
 * !important may be added to force override, but not necessary
 * #content is not necessary to hide scroll bars
 */
#TabsToolbar {visibility: collapse;}
#navigator-toolbox {visibility: collapse;}
browser {margin-right: -14px; margin-bottom: -14px;}
```

- Criar um atalho na área de trabalho que rode a linha de comando: `firefox -P <nome do perfil criado no segundo passo> <url do site que é para abrir>`
