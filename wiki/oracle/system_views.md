# Oracle

## Views de informação estática do banco de dados

- all_tables
- all_tab_columns
- all_users
- all_views
- all_procedures
- all_col_comments
- all_tab_comments
- all_constraints

## Queries

Todos as tabelas de usuário com comentários

```
select
  *
from
  all_tab_comments
where
    owner not in (
      'APEX_030200','CTXSYS','EXFSYS','MDSYS','OLAPSYS','SYS','SYSTEM','XDB'
    )
order by owner,table_name
```

Todos os campos de tabelas de usuário com comentários

```
select
  *
from
  all_col_comments
where
  owner not in (
    'APEX_030200','CTXSYS','EXFSYS','MDSYS','OLAPSYS','SYS','SYSTEM','XDB'
  )
order by owner,table_name,column_name
```

Devolve todos as constraints para todas as tabelas de usuário (terminar, referências: https://docs.oracle.com/cd/B19306_01/server.102/b14237/statviews_1037.htm#i1576022)

```
select
  owner || '.' || table_name as table_name,
  case constraint_type
    when 'C' then 'check'
    when 'P' then 'primary key'
    when 'U' then 'unique key'
    when 'R' then 'referential integrity'
    when 'V' then 'with check option'
    when 'O' then 'with read only'
    else 'unknown'
  end as kind
  
from
  all_constraints

where
  owner not in (
    'APEX_030200','CTXSYS','EXFSYS','MDSYS','OLAPSYS','SYS','SYSTEM','XDB', 'WMSYS', 'ORDDATA'
  )
  and owner='BKP'
  and table_name='S_EMPUSUPERF'

order by
  kind desc
```