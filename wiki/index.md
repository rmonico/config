# Índice de arquivos  

Índice de arquivos  

- [index](index.md)  
- [Markdown Reference](Markdown Reference.md)  

---


## [cruzeiro](cruzeiro/index.md)

- [banco_de_dados](cruzeiro/banco_de_dados.md)  
- [Cliente de Email](cruzeiro/Cliente de Email.md)  
- [configuracao_repositorio_repasse](cruzeiro/configuracao_repositorio_repasse.md)  
- [contas](cruzeiro/contas.md)  
- [Criação Ambiente de Desenvolvimento](cruzeiro/Criação Ambiente de Desenvolvimento.md)  
- [deploy](cruzeiro/deploy.md)  
- [Novo funcionário](cruzeiro/Novo funcionário.md)  
- [Tabelas](cruzeiro/Tabelas.md)  

---


## [dev](dev/index.md)

- [Hibernate e JPA - Principais classes e anotações](dev/Hibernate e JPA - Principais classes e anotações.md)  
- [Maven - Build do zero à Web](dev/Maven - Build do zero à Web.md)  
- [MySQL](dev/MySQL.md)  
- [Novo projeto com Python](dev/Novo projeto com Python.md)  
- [Programação Java para a Web - JSF](dev/Programação Java para a Web - JSF.md)  
- [Servlets](dev/Servlets.md)  
- [Spring Boot](dev/Spring Boot.md)  
- [tomcat](dev/tomcat.md)  

---


## [procedimentos](procedimentos/index.md)

- [entrevista](procedimentos/entrevista.md)  
- [manha](procedimentos/manha.md)  

---


## [sysadmin](sysadmin/index.md)

- [bluetooth](sysadmin/bluetooth.md)  
- [Configuração de Máquina](sysadmin/Configuração de Máquina.md)  
- [debug x startup](sysadmin/debug x startup.md)  
- [hosts](sysadmin/hosts.md)  
- [redirect_ssh](sysadmin/redirect_ssh.md)  
- [servicos](sysadmin/servicos.md)  
- [ssh](sysadmin/ssh.md)  

---


