# SSH

## Criar novas chaves de SSH

ssh-keygen -t rsa -b 8192 -C <rmonico@máquina>

Não esquecer de copiar a chave pública para a o `syncthing/ssh-keys/`!


## Usar chaves de SSH existente:

- Copiar o .ssh/id_rsa da instalação antiga.
- Linkar o id_rsa.pub com o arquivo adequado da pasta `syncthing/ssh-keys/`
