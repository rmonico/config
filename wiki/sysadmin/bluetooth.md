# Bluetooth no Archlinux

## Conectar à dispositivo já configurado e pareado  
```
bluetoothctl
# Depois de ligar o dispositivo  
$ connect <device id>

# Depois de sair do comando (interativo) acima com ctrl+D, configurar o volume com:
pavucontrol
```

## Problemas

O mais comum é o estado do adaptador de bluetooth não estar conforme o esperado pelo módulo. É possível resetar o estado reiniciando os serviços envolvidos:

```
sudo systemctl restart bluetooth.service
systemctl --user restart pulseaudio.service
```

Também é possível que o mpd não esteja enviando áudio para o dispositivo. Adicionar ao arquivo de configuração do mpd (`~/.config/mpd/mpd.conf`):

audio_output {
    type            "pulse"
    name            "pulse audio"
}


## Primeira configuração  

Instalar o `bluez` e o `bluez-utils` caso não estejam instalados  
```
sudo pacman -S bluez bluez-utils
```


Verificar existe algum dispositivo bluetooth instalado

```
lspci | grep -i bluetooth
lsusb | grep -i bluetooth
```

Verificar se o módulo de bluetooth existe e está carregado no kernel (4.1 ou superior)
```
lsmod | grep -i '^btusb'
```

Caso não esteja, carregar na memória  
Mas não tenho certeza de que se o módulo não for USB esse módulo vai servir....  
```
insmod btusb
```

Subir o serviço do bluetooth
```
sudo systemctl start bluetooth.service
```

Instalar o pacote pulseaudio-bluetooth
```
sudo pacman -S pulseaudio-bluetooth
```

Reiniciar o serviço do pulseaudio
```
systemctl --user restart pulseaudio
```

Configurar o adaptador do bluetooth e o dispositivo remoto (o comando a seguir é interativo, é possível completar com tab)  
Assim que rodar o comando tem que informar que o adaptador de bluetooth  
Houve uma vez que, após uma suspensão do sistema, não informou, se resolveu suspendendo o sistema novamente  
```
bluetoothctl
```

Ligar o adaptador bluetooth:
```
$ power on
```

Lista os dispositivos Bluetooth configurados
```
$ devices
```

Se o dispositivo desejado não estiver configurado é possível detectá-lo com (é necessário que o dispositivo remoto esteja ligado :-):
```
$ scan on
```

Parear com o dispositivo:
```
pair <device id>
```

Adicionar o dispositivo à lista de dispositivos confiáveis:
```
trust <device id>
```

É possível que o dispositivo esteja bloqueado:
```
unblock <device id>
```

Finalmente, conectar com o dispositivo:
```
connect <device ID>
```

Pode fechar o bluetoothctl com ctrl+D que tudo vai continuar funcionando

É possível que depois disso o dispositivo tenha sido pareado corretamente mas o volume esteja em 0%. É possível arrumar isso com:  
Se não tiver instalado, o nome do pacote também é pavucontrol  
```
pavucontrol
```

De volta ao shell, para ligar automaticamente o adaptador bluetooth ao ligar o computador:
```
echo AutoEnable=true >> /etc/bluetooth/main.conf
```

Testado com:  
Linux moningrado 4.4.71-1-MANJARO #1 SMP PREEMPT Wed Jun 7 19:25:52 UTC 2017 x86_64 GNU/Linux  


**Fonte:** https://wiki.archlinux.org/index.php/Bluetooth