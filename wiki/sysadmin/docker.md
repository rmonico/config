# Configurar cliente do docker hub:

- *(opcional)* Configurar proxy

Criar arquivo: `/etc/systemd/system/docker.service.d/http-proxy.conf`:

```
[Service]
Environment="HTTP_PROXY=10.128.131.16:3128"
Environment="HTTPS_PROXY=10.128.131.16:3128"
```

- Configurar certificados para serem usados pelo `dockerd`:

Colocar em: `/etc/docker/certs.d/<hostname:port>/` (como root)

Exemplo:

```
/etc/docker/certs.d/registry.viavarejo.com.br:5000/ca.cert
/etc/docker/certs.d/registry.viavarejo.com.br:5000/ca.key
/etc/docker/certs.d/registry.viavarejo.com.br:5000/ca.crt
```
