# Serviços

## Listar portas abertas

```
sudo netstat -tlpen | less 
```

## Celular (dontlookback)

Serviço        | Porta | URL                          | Notas
-              | -     | -                            | -
Syncthing      | 8384  | <https://dontlookback:8384/> | 
ADB sobre rede | 5555  | adb connect dontlookback     | Necessário habilitar no Android primeiro


## Moningrado


Serviço   | Porta | URL                        | Notas
-         | -     | -                          | -
Syncthing | 8384  | <https://moningrado:8384/> |

## Raspberry

Serviço   | Porta | URL                       | Notas
-         | -     | -                         | -
Syncthing | 8384  | <https://raspberry:8384/> |
Deluge    | 8112  | <https://raspberry:8112/> |
Kodi      | 8080  | <https://raspberry:8080/> |
