# Endereços de IP

## Máquinas

Máquina | SO | Descrição
- | - | -
moninquistao | Windows 10 | Máquina de trabalho do escritório
moninburgo | Manjaro | VM rodando no moninquistao
dontlookback | Android | Celular
moningrado | Manjaro | Notebook Avell
raspberry | Archlinux ARM | Raspberry
moninvostok | Ubuntu? | VPS na digital ocean


## Todas as informações  

status  | /etc/hosts (local) | /etc/hosts (vpn)          | Mac Address       | IP (papagaio)          | IP (VPN)  
-       | -                  | -                         | -                 | -                      | -  
ativo   | router             | --                        | --                | 10.0.0.1               | --  
inativo | moninvostok        | vpn_digitalocean          | --                | 104.236.127.20 **\*1** | 10.8.0.1 **\*3**  
ativo   | raspberry          | vpn_raspberry **\*2**     | B8-27-EB-47-D5-6C | 10.0.0.100             | 10.8.0.100 **\*3**  
ativo   | moningrado         | vpn_moningrado            | 64-5A-04-41-49-D5 | 10.0.0.101             | 10.8.0.101 **\*3**  
ativo   | dontlookback       | vpn_dontlookback          | 04-46-65-79-34-80 | 10.0.0.102             | 10.8.0.102 **\*3**  
ativo   | android_thais      | vpn_android_thais **\*2** | 88-79-7E-B9-C6-AA | 10.0.0.103             | 10.8.0.103 **\*3**  
ativo   | note_thais         | vpn_note_thais **\*2**    | **Não sei...**    | 10.0.0.104 **\*3**     | 10.8.0.104 **\*3**  
inativo | noteazul           | vpn_noteazul **\*2**      | **Não sei...**    | 10.0.0.105 **\*3**     | 10.8.0.105 **\*3**  
inativo | tablet_mãe         | vpn_tablet_mae **\*2**    | **Não sei...**    | 10.0.0.106 **\*3**     | 10.8.0.106 **\*3**  

*\*1: IP da internet, não é da rede local*  
*\*2: Arquivos hosts não configurado*  
*\*3: IPs não configurados*  

## Hosts

###Localização

SO                                      | Path
:-                                      | :- 
Linux                                   | `/etc/hosts`  
Android                                 | `/system/etc/hosts`  
Windows (2000, NT, XP, Vista, 7, 8, 10) | `C:\Windows\System32\Drivers\etc\hosts`  
Mac OS                                  | `/private/etc/hosts`  

**Observação**: Não testei no Android e no Windows.  

###Conteúdo:

    # Manual
    
    # Internet
    104.236.127.20    moninvostok vpnserver_change_ip_on_etc_hosts
    
    # Papagaio
    10.0.0.1          router
    10.0.0.100        raspberry
    10.0.0.101        moningrado
    10.0.0.102        dontlookback
    10.0.0.103        celular_thais
    10.0.0.104        note_thais
    10.0.0.105        noteazul
    10.0.0.106        tablet_mãe
    
    # VPN
    10.8.0.1      vpn_digitalocean
    10.8.0.100    vpn_raspberry
    10.8.0.101    vpn_moningrado
    10.8.0.102    vpn_dontlookback
    10.8.0.103    vpn_android_thais
    10.8.0.104    vpn_note_thais
    10.8.0.105    vpn_noteazul
    10.8.0.106    vpn_tablet_mae

