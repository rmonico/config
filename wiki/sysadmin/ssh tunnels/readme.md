# Cria túneis de SSH para os serviços mais usados remotamente (SSH, VNC e Syncthing)

```
cp -v *.service "$HOME/.config/systemd/user/"
systemctl --user enable vnc_tunnel.service syncthing_tunnel.service ssh_tunnel.service
systemctl --user start vnc_tunnel.service syncthing_tunnel.service ssh_tunnel.service
```

*Não esquecer de mudar o número das portas nos arquivos de acordo com a máquina onde estiver sendo instalado!*
