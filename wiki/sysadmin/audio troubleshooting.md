# Áudio troubleshooting

- Tirar e colocar de volta o plug de áudio

- Ver se o player está rodando e reproduzindo

`mpc`

- Ver se o som está no mudo

`alsamixer`

- pulseaudio?

- Restaurar o estado original do alsa

`sudo systemctl force-restart`

