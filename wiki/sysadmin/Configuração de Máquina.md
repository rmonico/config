# Configuração de Máquina  


## Instalar base para configurar o resto:


```
# Atualizar o sistema (demorado)
yaourt --noconfirm -Syua

# Instalar o mínimo:
sudo pacman -S --noconfirm git zsh zsh-theme-powerlevel9k
yaourt -S --noconfirm nerd-fonts-complete j4-make-config-git

#Instalar o oh-my-zsh:
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh  

# Criar pasta de temas do oh-my-zsh:
mkdir $HOME/.oh-my-zsh/custom/themes

# Criar link para o tema instalado pelo yaourt:
ln -s /usr/share/zsh-theme-powerlevel9k .oh-my-zsh/custom/themes/powerlevel9k

# Gerar chave de ssh para o bitbucket:
ssh-keygen -b 8192 -t rsa -f $HOME/.ssh/id_rsa-$(cat /etc/hostname)-bitbucket

# Clonar a pasta sincronizada:
git clone git@bitbucket.org:rmonico/data.git $HOME/data  

# Montar os links para os arquivos de configuração:
exec $HOME/dados/config/make_links

# Mudar o shell para o zsh
sudo usermod -s "$(grep zsh /etc/shells | head -n 1)" rmonico
```

Reiniciar o gerenciador de janelas para as alterações terem efeito

## ssh  

Criar links para arquivos comuns:  

```  
cd ~/.ssh  
# Defasado: não estão mais no syncthing essas informações
ln -s ../syncthing/ssh-keys/authorized_keys  
ln -s ../syncthing/ssh-keys/known_hosts  
ln -s ../data/
```  


## Instalar Pacotes dos repositórios  

```
# Depois que montei a seção inicial não tirei daqui as coisas que já foram instaladas por lá
sudo pacman -S --noconfirm xclip kdiff3 chromium retext arc-themes-maia variety python-virtualenv python-virtualenvwrapper task youtube-dl freemind syncthing syncthing-gtk keepassxc zsnes mupen64plus easystroke lxterminal zsh-theme-powerlevel9k maven irssi cantata mpd mpc gradle ipython mplayer git xscreensaver redshift pavucontrol evince subdownloader wesnoth tk libreoffice-fresh feh android-studio beruski berusky2 openarena spotify rofi vinagre gtk3-print-backends jdk8-openjdk
```

```
yaourt -S --noconfirm tuxjdk intellij-idea-ultimate-edition openwmail whatsie tmsu xbanish franz qdirstat j4-make-config-git sublime-text-dev jdk8
```

*Observações:*  
`feh`: Necessário para o variety funcionar, ver se o bug do pacote já foi resolvido  
`tk`: Necessário para funcionar o `gitk` e o `git-gui`  
`zsh-theme-powerlevel9k`: O zsh já está vindo por default no Manjaro, por isso não está aqui  
`jdk8-openjdk`: Testando a tuxjdk  
`skype`: Testando o franz-bin do AUR  
`gtk3-print-backends`: para os aplicativos baseados em GTK3 enxergarem as impressoras do sistema  


Instalar o Eclipse manualmente
*Se possível pegar pasta `programs` de outra instalação, é muita coisa.*  


## Pastas Especiais  

Logs do usuário:  

```  
sudo mkdir /var/log/rmonico  
sudo chown rmonico:rmonico /var/log/rmonico  
ln -s /var/log/rmonico logs  
```  


[Criar novas chaves / Linkar com chaves de SSH existentes](sysadmin/ssh.md)  

## MPD  

Criar a pasta de configuração do MPD:  

ln -s ~/syncthing/config/mpd  

Adicionar usuário `rmonico` ao grupo `audio`:  

usermod -aG audio rmonico  
`<sair da sessão e entrar novamente para esta configuração ter efeito>`  


## Java  

Maven: se disponível, copiar o .m2 de outra instalação (demora muito pra baixar tudo de novo pelo maven)  


## Rede  

Adicionar o <hosts.md> ao `/etc/hosts`  


## Syncthing

```
sudo systemctl enable syncthing@rmonico.service  
sudo systemctl start syncthing@rmonico.service  
```


## Mudar o browser padrão para o chromium

Criar o arquivo `$HOME/.local/share/application/chromium.desktop`

```
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=Chromium
Exec=chromium
```

Rodar:

```
xdg-settings set default-web-browser chromium.desktop
```

*Observação: Se a variável `$BROWSER` existir o comando não funcionará.*
