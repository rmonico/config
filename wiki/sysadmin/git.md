# GIT

## Repositório de git em vboxsf

Ignorar bits de execução nos arquivos  

```
core.filemode=false
```

Converte finais de linha para CRLF quando faz checkout de alguma coisa  

```
core.autocrlf=true
```

## Outros

Definir identidade default do ssh de um repositório (substitui o `export GIT_SSH_COMMAND`)  

```
core.sshCommand="ssh -i <chave privada de SSH>"
```

### git reset HEAD^

Volta para o commit anterior. Caso o commit atual seja de merge volta respeitando a branch atual.


### Sub-árvore movida:

```
git merge -s recursive -Xsubtree=<sub pasta> <branch para ser mergeada>
```
