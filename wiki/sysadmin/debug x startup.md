# Debug de sessão do X

Inicialização das variáveis:

```
log="[[ -f /home/rmonico/__debug__ ]] && (echo \"[\$(date '+%F %T')] \$(cat -)\") >> /home/rmonico/scripts_ran"
start="echo \$0 started \(\$@\) | eval \$log"
finish="echo \$0 finished | eval \$log"
```

```
eval "$start"
echo "On .xprofile" | eval "$log"
```

Final do script:

```
echo "On .xprofile" | eval "$log"
eval "$finish"
```

## Principais arquivos de inicialização

- `$HOME/.xprofile`: Executado quando o X é inicializado
- `$HOME/.xinitrc`: Executado quando uma nova sessão do X é iniciada (deve rodar um window manager, costuma receber um argumento que é a sessão que deve ser inicializada)
- `$HOME/.xmodmaprc` [opcional]: Arquivo de inicialização com as configurações de tecla (não necessariamente de layout de teclado)
- `$HOME/.Xresources`: Arquivo de inicialização do ambiente do X
- `$HOME/.zshrc`: Executado quando uma sessão interativa do ZSH é iniciada
- `/etc/fstab`: Montagens automaticas durante a inicialização (ou via `mount -a`)
