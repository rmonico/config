# Grub

- Arquivo principal de configuração: `/etc/default/grub`

Este arquivo é usado para gerar as configurações que serão lidas pelo Grub na hora do boot (que é o `/etc/boot/grub`). Depois de fazer alguma alteração nesse arquivo será necessário atualizar os demais arquivos de configuração com:

`grub-mkconfig -o /boot/grub/grub.cfg`

## Referências:

- https://wiki.archlinux.org/index.php/Grub#Generate_the_main_configuration_file