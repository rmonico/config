# Redirect de SSH

```
ssh -N -R <remote_port_to_listen_on>:localhost:<local_port_to_redirect_to> user@host  
```

Instrui o servidor de SSH rodando em `host` a abrir a porta `remote_port_to_listen_on` e redirecionar todo o tráfego para `localhost` que por sua vez vai redirecionar, para a porta (em `localhost`) `local_port_to_redirect_to`.  
Pode ser finalizado com `Ctrl+C`  

*Observação:*

O -N instrui o SSH a não abrir sessão interativa, é útil apenas nesse caso.

**todo**: estudar como colocar isso para iniciar automaticamente com o server de ssh  