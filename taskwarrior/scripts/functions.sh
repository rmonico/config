task_help() {
cat << FIM
Taskwarrior functions:

t: 'task "\$@"'
ta: 'task add "\$@"', checks for rc.dateformat errors and retry with $TASKWARRIOR_ALT_DATEFORMAT
tl: 'task rc.defaultwidth:$COLUMNS "\$@"', calls a report using 'task' with pager (less), when needed (saves the output to temp file)
tlast: Return last created task uuid. If \"-s\" is passed show last task
ts: task search. Assembly a string in form: '\bparam1\b or \bparam2\b'. Uses 'tl' function to show results.
tan: 'task "\$1" annotate "<remaining parameters>"', if \"$1\" is not a number defaults to \"tlast\" (uuid of last created task)
tn: 'tasknote "\$@"'
tdone: 'task "\$@" done' Warning: doesn't check is parameters are valid! 
tdel: 'task "\$@" delete' Warning: doesn't check is parameters are valid!
tm: 'task "\$1" mod "<remaining parameters>"', checks for rc.dateformat errors and retry with $TASKWARRIOR_ALT_DATEFORMAT
tc: 'task context "\$@"'
tu: 'task undo'
FIM
}

unalias t &> /dev/null
t() {
    [ -n "$TW_CONTEXT" ] && local _params=(rc.context:$TW_CONTEXT)

    task ${_params[@]} "$@"
}

ta() {
    stderr="$(mktemp)"

    task add "$@" 2> "$stderr"

    initial_command_return_code=$?

    dump_stderr=1

    if [ $initial_command_return_code -eq 2 ]
    then
        grep -E "'.*' is not a valid date in the '.*' format." "$stderr" > /dev/null

        # Error is dateformat related? Can I handle it?
        if [ $? -eq 0 -a -n "$TASKWARRIOR_ALT_DATEFORMAT" ]
        then
            echo "Detected error related to 'dateformat' configuration. Retrying with TASKWARRIOR_ALT_DATEFORMAT variable..." > /dev/stderr

            task "rc.dateformat:$TASKWARRIOR_ALT_DATEFORMAT" add "$@"

            retry_returned_code=$?

            dump_stderr=0
        fi
    fi

    if [ $dump_stderr -eq 1 ]
    then
        cat "$stderr" > /dev/stderr
        rm "$stderr"
        return $initial_command_return_code
    fi

    rm "$stderr"
    return $retry_returned_code
}

tl() {
    local output_file="$(mktemp)"

    task rc.defaultwidth:$COLUMNS "$@" > "$output_file"

    local output_line_count="$(cat "$output_file" | wc -l)"

    local prompt_height=2

    if [ "$output_line_count" -ge "$((LINES - prompt_height - 1))" ]
    then
        less "$output_file"
    else
        cat "$output_file"
    fi

    rm "$output_file"
}

tlast() {
  uuid=$(task _uuids +LATEST)

 [[ "$1" == "-s" ]] && task $uuid || echo $uuid
}

ts() {
  local params=()

  for ((i=1; i<$#@+1; i++))
  do
    params+=( \\b"$@[$i]"\\b )
    
    if [ $i -lt $(($#@)) ]
    then
      params+=( or )
    fi
  done

  tl "${params[@]}"
}

tan() {
  # Check first parameter is a number
  # if eval '[ "$1" -eq "$1" ]' &> /dev/null
  if [ "$1" -eq "$1" ] &> /dev/null
  then
    id="$1"
    shift 1
  else
    id=$(tlast)
  fi

  t $id annotate "$@"
}

tn() {
    tasknote "$@"
}

tdone() {
    task "$@" done
}

tdel() {
    task "$@" delete
}

tm() {
    local params=()
    local id="$1"

    for ((i=2; i<$#@+1; i++))
    do
    params+="$@[$i]"
    done

    stderr="$(mktemp)"

    t "$id" mod "${params[@]}" 2> "$stderr"

    initial_command_return_code=$?

    dump_stderr=1

    if [ $initial_command_return_code -eq 2 ]
    then
        grep -E "'.*' is not a valid date in the '.*' format." "$stderr" > /dev/null

        # Error is dateformat related? Can I handle it?
        if [ $? -eq 0 -a -n "$TASKWARRIOR_ALT_DATEFORMAT" ]
        then
            echo "Detected error related to 'dateformat' configuration. Retrying with TASKWARRIOR_ALT_DATEFORMAT variable..." > /dev/stderr

            t "rc.dateformat:$TASKWARRIOR_ALT_DATEFORMAT" "$id" mod "${params[@]}"

            retry_returned_code=$?

            dump_stderr=0
        fi
    fi

    if [ $dump_stderr -eq 1 ]
    then
        cat "$stderr" > /dev/stderr
        rm "$stderr"
        return $initial_command_return_code
    fi

    rm "$stderr"
    return $retry_returned_code
}

tc() {
    task context "$@"
}

trep() {
    initial_folder="$(pwd)"

    cd "$TASKWARRIOR_SCRIPTS_FOLDER"

    python -m reports "$@"

    cd "$initial_folder"
}

# tg() {
#     tl tags
# }

# tp() {
#     tl projects
# }

tu() {
  task undo
}
