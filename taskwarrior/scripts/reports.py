#!/usr/bin/env python3
# coding=utf-8

import argparse
import taskwarrior_wrapper
from _flags import __DEBUG__
import datetime

COMMANDS = ['today', 'week', 'lastweek',
            'nextweek', 'month', 'lastmonth', 'nextmonth']


def main():
    global parser
    parser = argparse.ArgumentParser(
        description='Taskwarrior complex reports made simple')

    parser.add_argument('command', choices=COMMANDS)
    parser.add_argument('remainder', nargs=argparse.REMAINDER, metavar='args')

    global args
    args = parser.parse_args()

    if __DEBUG__:
        print('command: {}'.format(args.command))
        print('args: {}'.format(args.remainder))

    if args.command == 'today':
        taskwarrior_wrapper.report(["due:today"] + args.remainder)
    elif args.command == 'week':
        # Start of week: sunday, 00:00, end: saturday, 23:59
        today = datetime.date.today()
        last_sunday = today - datetime.timedelta(today.weekday() + 1)

        week_end = last_sunday + datetime.timedelta(6)

        taskwarrior_wrapper.report_by_period(
            start=last_sunday, end=week_end, args=args.remainder)
    elif args.command == 'lastweek':
        today = datetime.date.today()
        last_sunday = today - \
            datetime.timedelta(today.weekday() + 1) - datetime.timedelta(7)

        week_end = last_sunday + datetime.timedelta(6)

        taskwarrior_wrapper.report_by_period(
            start=last_sunday, end=week_end, args=args.remainder)
    elif args.command == 'nextweek':
        today = datetime.date.today()
        last_sunday = today - \
            datetime.timedelta(today.weekday() + 1) + datetime.timedelta(7)

        week_end = last_sunday + datetime.timedelta(6)

        taskwarrior_wrapper.report_by_period(
            start=last_sunday, end=week_end, args=args.remainder)
    elif args.command == 'month':
        today = datetime.date.today()

        start_of_current_month = datetime.date(today.year, today.month, 1)

        a_day_in_next_month = start_of_current_month + datetime.timedelta(31)

        end_of_current_month = datetime.date(
            a_day_in_next_month.year, a_day_in_next_month.month, 1) - datetime.timedelta(1)

        taskwarrior_wrapper.report_by_period(
            start=start_of_current_month, end=end_of_current_month, args=args.remainder)
    elif args.command == 'lastmonth':
        today = datetime.date.today()

        start_of_current_month = datetime.date(today.year, today.month, 1)

        end_of_last_month = start_of_current_month - datetime.timedelta(1)
        start_of_last_month = datetime.date(
            end_of_last_month.year, end_of_last_month.month, 1)

        taskwarrior_wrapper.report_by_period(
            start=start_of_last_month, end=end_of_last_month, args=args.remainder)
    elif args.command == 'nextmonth':
        today = datetime.date.today()

        a_day_in_next_month = today + datetime.timedelta(31)

        start_of_next_month = datetime.date(
            a_day_in_next_month.year, a_day_in_next_month.month, 1)

        a_day_in_another_month = start_of_next_month + datetime.timedelta(31)

        end_of_next_month = datetime.date(
            a_day_in_another_month.year, a_day_in_another_month.month, 1) - datetime.timedelta(1)

        taskwarrior_wrapper.report_by_period(
            start=start_of_next_month, end=end_of_next_month, args=args.remainder)


if __name__ == '__main__':
    main()
