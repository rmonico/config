#!/usr/bin/env python3
# coding=utf-8

from _flags import __DEBUG__

import shutil
import subprocess
import datetime


def binary():
    return shutil.which("task")


def report(args):
    columns = shutil.get_terminal_size(fallback=(0, 25)).columns
    arguments = ['rc.defaultwidth:{}'.format(columns)] + args

    if __DEBUG__:
        print("Running taskwarrior for report with args: {}".format(arguments))

    subprocess.run(arguments, executable=binary())


def report_by_period(start, end, args=[]):
    last_minute_before_start = datetime.datetime(start.year, start.month, start.day) - datetime.timedelta(minutes=1)
    first_minute_after_end = datetime.datetime(end.year, end.month, end.day) + datetime.timedelta(days=1)

    start_str = last_minute_before_start.strftime('%d/%m/%yT%H:%M')
    end_str = first_minute_after_end.strftime('%d/%m/%yT%H:%M')

    period_filter = ['rc.dateformat:d/m/yTH:N',
                     'due.after:' + start_str, 'and', 'due.before:' + end_str]

    if args != []:
        period_filter += ['and']

    report(period_filter + args)
