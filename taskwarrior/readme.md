# Repositório do Taskwarrior


## Relatórios

*É possível pegar essa lista com o comando `tw_reports`


## Contextos

acompanhar      Assuntos em aberto
carreira        Tarefas profissionais de longo prazo
casa            Afazeres de casa
dev             Sistemas em desenvolvimento
feira           Coisas para comprar na feira
grana           Itens envolvendo dinheiro
limpeza         Limpezas (dados, contatos, bookmarks, etc)
organizacao     Melhoria da organização
pessoal         Coisas pessoais
rua             Coisas para fazer na rua
saude           Saúde
setup           Configuração do computador
trampo          Trabalho
1-1             Itens encaminhados das reuniões de 1:1 do trabalho


## Tags

acompanhar      Assuntos em aberto
hoje            Terminar até o fim do dia
semana          Terminar até o fim da semana
rapido          Coisas que dá pra fazer em menos de 15 minutos
rua             Rua
feira
farmacia
mercado
longo           Coisas que precisam de muitos dias (ou semanas) para terminar


## Organização

foco                    Tarefa em andamento
rapido (tag)            Tarefas que podem ser finalizados em até 15 minutos
due (property)          Tarefa agendada com data limite
scheduled (property)    Tarefa agendada

