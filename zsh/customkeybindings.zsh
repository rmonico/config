# vim: filetype=zsh

# alt+l -> ' | less'
bindkey -s '^[l' ' | less'

# alt+L -> ' | less' + return
bindkey -s '^[L' ' | less\n'

# alt+s -> 's'
bindkey -s '^[s' 's\n'

# alt+shift+s -> 'sa'
bindkey -s '^[S' 'sa\n'

# alt+j -> ' | jq -C . | less' + return
bindkey -s '^[j' ' | jq -C . | less'

# alt+u -> 'cd .. + s'
bindkey -s '^[u' 'cd ..\ns\n'

# alt+g -> ' | grep'
bindkey -s '^[g' ' | grep '

# alt+n -> 'navi' + return
bindkey -s '^[n' 'navi\n'

# alt+- -> 'cd -'<tab>
bindkey -s '^[-' 'cd -\t'

# alt+_ -> 'cd -'<return>
bindkey -s '^[_' 'cd -\n'

# alt+t -> 'tests' + return
bindkey -s '^[t' 'tests\n'

bindkey '^R' history-incremental-pattern-search-backward
bindkey '^S' history-incremental-pattern-search-forward


