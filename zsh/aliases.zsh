# vim: filetype=zsh

# Manjaro by default has a gk alias, I have a script called gk
unalias gk &> /dev/null

# git aliases
unalias g &> /dev/null
alias g=git

alias gs="git status"

alias ga="git add"
alias gap="git add --patch"
alias gaa="git add -A"

unalias gca &> /dev/null
alias gca="git commit --amend"
alias gcan="git commit --amend --no-edit"


git-conventional-commit() {
    local kind="$1"
    shift 1
    local branch="$(git branch --no-color | get_branch)"

    local message="${kind}(${branch}): ${@}"

    git commit --message "$message"
}

alias gcbuild="git-conventional-commit build"
alias gcchore="git-conventional-commit chore"
alias gcfeat="git-conventional-commit feat"
alias gcfix="git-conventional-commit fix"
alias gcdocs="git-conventional-commit docs"
alias gcrefactor="git-conventional-commit refactor"
alias gcperf="git-conventional-commit perf"
alias gcstyle="git-conventional-commit style"
alias gctest="git-conventional-commit test"



git-conventional-commit() {
    local kind="$1"
    shift 1
    local branch="$(git branch --no-color | get_branch)"

    local message="${kind}(${branch}): ${@}"

    git commit --message "$message"
}

alias gcbuild="git-conventional-commit build"
alias gcchore="git-conventional-commit chore"
alias gcfeat="git-conventional-commit feat"
alias gcfix="git-conventional-commit fix"
alias gcdocs="git-conventional-commit docs"
alias gcrefactor="git-conventional-commit refactor"
alias gcperf="git-conventional-commit perf"
alias gcstyle="git-conventional-commit style"
alias gctest="git-conventional-commit test"


alias gd="git diff"
alias gds="git diff --staged"

unalias gst &> /dev/null

alias gst="git stash"
alias gstl="git stash list"
alias gsts="git stash show"
alias gstp="git stash --patch"
alias gsta="git stash apply"

alias gb="git branch"
alias gba="git branch --color --all"
alias gbv="git branch -vvv"

alias gw="git switch"
alias gwd="git switch --detach"
alias gwc="git switch -C"

# Antigo git reset
alias gr="git restore"
alias grp="git restore --patch"

alias grs="git restore --staged"
alias grsp="git restore --staged --patch"

unalias gp &> /dev/null

# "patchs" deve estar no PATH
alias gp="git patch"

alias gwt="git worktree"
alias gwtl="git worktree list"
alias gwta="git worktree add"
alias gwtr="git worktree remove"

alias d=docker
# alias dc="docker-compose"
alias di="docker image"
# alias dr="docker run"

# general commands aliases
# -rmlastnl: doesn't copy last character if its a new line
alias xclip="xclip -rmlastnl -sel clip"
alias patchdiff="git -c core.pager= diff --no-prefix --no-color"
alias grepc="grep --color=always"
alias egrepc="egrep --color=always"
alias lsblk="lsblk --output name,size,ro,type,label,fstype,fssize,fsavail,mountpoint"
alias open="xdg-open"
alias edit="$EDITOR"
alias vedit="$VISUAL_EDITOR"
alias df="df --human-readable"

if which tree &> /dev/null; then
    unset -f tree &> /dev/null
    export _tree="$(realpath $(which tree))"
    tree() {
        $_tree -C "$@" | less
    }
fi

s() {
    lsd --long "$@" | less
}

c() {
    if [[ "$1" == -* ]]; then
        cd_alias "$@"
    else
        local folder="$([ -d "$@" ] && echo "$@" || cd_alias "$@")"

        if [ ! -e "$folder" ]; then
            echo "Pasta ou alias inexistente: \""$@"\""
            return 1
        fi

        cd "$folder"

        [ -e .git ] && git status || s
    fi
}

alias sa="s -A"
alias sg="s -A | grep"
alias st="s --tree"

