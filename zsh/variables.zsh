# vim: filetype=zsh

# Variables for new zsh sessions
export LS_DEFAULT_PARAMS=(-l --group-directories-first --color)
export WGET_DEFAULT_PARAMS="--continue --timeout=10 --tries=inf --waitretry=10"

export TASKWARRIOR_ALT_DATEFORMAT="d/bTH:N"
export TASKWARRIOR_SCRIPTS_FOLDER="$HOME/.task/scripts"

# Less Colors for Man Pages
# export LESS_TERMCAP_mb=$'\E[01;31m'
# export LESS_TERMCAP_md=$'\E[01;38;5;74m'
# export LESS_TERMCAP_me=$'\E[0m'
# export LESS_TERMCAP_se=$'\E[0m'
# export LESS_TERMCAP_so=$'\E[01;47;34m'
# export LESS_TERMCAP_ue=$'\E[0m'
# export LESS_TERMCAP_us=$'\E[04;38;5;146m'
export LESS_TERMCAP_mb=$'\E[01;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[01;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[01;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

# Default less params
export LESS=-ISFRrXx4

# VirtualenvWrapper
export WORKON_HOME="$HOME/.virtualenvs"

# Fix Freemind fonts
# Do this individually on every program
# export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=truedd'

# TODO Era interessante ter um esquema para determinar esse formato em função do remote do repositório
# Assai
export GIT_COMMIT_MESSAGE_FORMAT='"${GIT_MESSAGE_PREFIX} - $@"'
# Meu
# export GIT_COMMIT_MESSAGE_FORMAT='"[${GIT_MESSAGE_PREFIX}] $@"'

export IPDB_CONTEXT_SIZE=20
export PYTHONBREAKPOINT=ipdb.set_trace

# https://linux.die.net/man/1/grotty
export GROFF_NO_SGR=1

