# vim: filetype=zsh

_clipboard_paste() {
    local contents="$(which copyq &> /dev/null && copyq clipboard || echo "****'copyq' not found on PATH****")"

    if [ -n "$contents" ]; then
        zle -U "$contents"
    fi
}

# _clipboard_copy() {
#     zle yank "$(xclip -i)"
# }

zle -N clipboard_paste _clipboard_paste
zle -N clipboard_copy _clipboard_copy

# ctrl+p -> paste bindkey "^V" quoted-insert
bindkey '^p' clipboard_paste
# bindkey '^P' clipboard_copy

