# vim: filetype=zsh

function next() {
  if [ $# != 1 ]; then
    echo "Usage: next <id to make next task>"
    return 1
  fi

  local current_ids=($(task ids +next))

  if [ $#current_ids -ne 0 ]; then
    task $current_ids mod -next
  fi

  task $1 mod +next
}

function copp() {
    pwd | xclip -sel clip && echo Path copied to clipboard || echo Erro coping path to clipboard
}

# zsh has a built-in alias for gc
unalias gc 2> /dev/null

function gc() {
    if [ -n "$GIT_COMMIT_MESSAGE_FORMAT" -a -n "$GIT_MESSAGE_PREFIX" ]; then
        message="$(eval echo \"${GIT_COMMIT_MESSAGE_FORMAT}\")"
    else
        message="$@"
    fi

    git commit -m "$message"
}

unalias gcb 2> /dev/null

function gac() {
    git add .
    git commit -am "$@"
}

# Git Graphical Log
function ggl() {
    bash -c "nohup gitk --all $@ &> /dev/null" &
}

function glog() {
    local author_cols_after=0
    local date_cols_after=16

    # right alignment stuff...
    local _author_cols_after=$(($COLUMNS - $author_cols_after))
    local _date_cols_after=$(($COLUMNS - $author_cols_after - $date_cols_after))

    # echo $author_cols_after
    # echo $date_cols_after

    # echo $_author_cols
    # echo $_date_cols
    git log --all --graph "--pretty=format:%Cgreen%d%Creset %s %Cblue%>|($_date_cols_after)%ah%Creset %Cgreen%>|($_author_cols_after)%an%Creset" "$@"
}

function trim() {
    sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

function cppath() {
  if [ "$(uname)" = 'CYGWIN_NT-10.0' ]; then
    # BUG: return path with a \n at end
    # TODO: get a parameter with relative path
    cygpath -w $(pwd) > /dev/clipboard
  else
    pwd | xclip
  fi 
}

function pomodoro() {
  if [ "$1" = "help" -o "$1" = "-h" -o "$1" = "--help" ]; then
cat << EOF
Usage:

  pomodoro <pomodoro count, default 4> <pomodoro time, in minutes, default 25>

Its also possible use variables "pomodoro_count" and "pomodoro_time".
EOF
    return 0
  fi

  local _pomodoro_count=${1:-${pomodoro_count:-4}}
  local _pomodoro_time=${2:-${pomodoro_time:-25}}

  for ((pomodoro=1; pomodoro <= ${_pomodoro_count:-4}; pomodoro++)); do
    echo "Starting pomodoro $pomodoro / $_pomodoro_count...."
    sleep $((_pomodoro_time * 60));
    [[ $pomodoro -lt $_pomodoro_count ]] && zenity --info "--title=Pomodoro $pomodoro/$pomodoro_count:" "--text=Pomodoro finished\!\n\nTime to take  break.\nClose this dialog when done.";
  done

  zenity --info "--title=Pomodoro session complete" "--text=Pomodoro session complete.\n\nTake a long break (30 min)"
}

function up() {
  local directory_count=1

  local print_only='no'
  local help_only='no'

  local param
  local flag

  for param in $@; do
    for flag in print p -p --p; do
      [ "$param" = $flag ] && print_only=yes
    done

    for flag in usage help h -h --help; do
      [ "$param" = $flag ] && help_only=yes
    done

    local _check=99

    [ $(($_check + $param)) -gt 99 ] && directory_count=$param
  done

  if [ $help_only = 'yes' ]; then
    cat << EOF
Goes up n directories or print the amount ../ need to go to it.

Examples:
up            # same as cd ..
up 3          # same as cd ../../../
up print 3    # same as echo ../../../
up help       # Show this help message

"print" can be replaced with "p", "-p" or "--print"
EOF
    return 0
  fi

  local i
  local x

  for i in $(seq ${directory_count:-1}); do
    x="$x../"
  done

  [ $print_only = yes ] && echo $x || cd $x
}

function make_worktree_for_card() {
    if [ "$1" == "-h" -o "$1" == "--help" -o "$#" -eq 0 ]; then
        cat << EOF
Uso: <card> <branch> [remote]

Cria, localmente, uma branch com nome "feature/<card>-<branch>" e uma worktree para essa branch um nível acima da pasta atual.
A branch é criada a partir da branch ativa.
Se <remote> for informado a branch também criada remotamente.

card: Número do card no Azure (ex: 14569)
branch: nome base para a branch (ex: perfilservice)
remote: nome do remote (ex: origin)
EOF
        return 0
    fi

    card=$1
    name=$2
    remote=$3

    branchname=feature/${card}-${name}
    upstream=$branchname
    folder=../${name}

    git branch -c $branchname
    git worktree add $folder $branchname

    [ -n "$remote" ] && git push $remote --set-upstream feature/$branchname
}

if [ -f "$TASKWARRIOR_SCRIPTS_FOLDER"/functions.sh ]; then
    source "$TASKWARRIOR_SCRIPTS_FOLDER"/functions.sh
fi

