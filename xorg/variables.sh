# vim: filetype=bash

export TERMINAL="alacritty"

export LOGDIR="/var/log/${USER}"

export BROWSER="$(which firefox)"

export PATH="$HOME/bin:$HOME/binvia:$HOME/bin/__machine_specific__:$HOME/bin/stubs:$HOME/.gem/ruby/2.7.0/bin:$PATH"

export PYTHONPATH="$PYTHONPATH:$HOME/bin/python"

# export IDEA_JDK="/usr/lib/jvm/jdk-jetbrains"

# Fixes problem on variety 
export XDG_CURRENT_DESKTOP="i3"

export CONFIG="$HOME/config"
export BIN="$HOME/bin"
export SYNC="$HOME/syncthing"
export REPOS="$HOME/repos"
export DBS="$REPOS/dbs"
export MUSICS="$HOME/music"

export YOUTUBEDL_DEFAULT_PARAMS=(--output "%(autonumber)02d - %(title)s.%(ext)s")
export YOUTUBEDL_PARAMS_FOR_AUDIO=(--format worstvideo+bestaudio --extract-audio --audio-format mp3)
export YOUTUBEDL_WRAPPER_FOLDER="$HOME/youtube"

export NVIM_LISTEN_ADDRESS="/tmp/nvim"
export NVR_CMD="/usr/bin/gnvim"

export EDITOR="nvim"
export VISUAL_EDITOR="nvr --remote"
export ALTERNATIVE_VISUAL_EDITOR="subl"

export PIP_DOWNLOAD_CACHE=~/.pip_download_cache

export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# export _JAVA_OPTIONS='-Dswing.aatext=TRUE -Dawt.useSystemAAFontSettings=on'

export LANG=pt_BR.UTF-8

