#!/bin/sh


# Skel variables
script_name="$0"
initial_folder="$(pwd)"
# Used which and cd just for this script work, you dont need to put built-in commands here :-p
dependencies=(tmux notify-send)

# Default optional options values
job_name='Slow operation'
ok_message='Command executed sucessfully!'
error_message='Command finished with errors. Code: $?'
verbose="no"

# Required parameters
# set max to any negative number for an arbitrary default parameter count
default_parameter_max=-1
default_parameter_min=1
default_parameters=()

# TODO Options from config file
# TODO Log file options -- Dar uma olhada no commando logger, só não encontrei onde ele dá saída (além do stderr)
# TODO If a option is defined more than one time should stop

# usage [ exit_code [ error_message ] ] 
function usage() {
    if [ $# -eq 2 ]; then
        echo
        echo "Error: $2"
    fi

    # TODO: Criar um parâmetro --after que é um comando pra rodar depois do comando e da notificação
    # TODO: Criar parâmetros --stdout --stderr para dizer onde tem que salvar a saída do arquivo
    # TODO: Criar um parâmetro para escolher o notificador (zenity ou notify-send)
    # TODO: Criar parâmetros para os títulos das mensagens
    # TODO: Criar parâmetro para o nome da tarefa
    # TODO: Mudar o nome do script para algo como background_job
    echo
    echo "$script_name [ -m | --ok-message message ] [ -e | --error-message message ] [ -v | --verbose ] [ -h | --help ] command"
    echo
    echo " -j, --job-name        Name of the job, used on notification messages"
    echo " -m, -o, --ok-message  Message to show when done"
    echo " -e, --error-message   Message to show if commands doesn't return 0"
    echo " -h, --help            This help message"
    echo " -v, --verbose         Enable verbosity"
    echo " command               Command to be run in background (by tmux)"
    echo
    echo "Dependencies:"
    echo
    echo "The commands: ${dependencies[@]} must be available!"
    echo

    # TODO Examples
    if [ $# -gt 0 ]; then
        exit $1
    fi
}

function parse_command_line() {
    local default_parameters_found=0

    while [ $# -gt 0 ]; do
        case "$1" in
            "-j" | "--job-name")
                job_name="$2"
                shift 2
            ;;

            "-m" | "-o" | "--ok-message")
                ok_message="$2"
                shift 2
            ;;

            "-e" | "--error-message")
                error_message="$2"
                shift 2
            ;;

            "-v" | "--verbose")
                verbose="yes"
                shift 1
            ;;

            "-h" | "--help")
                usage 0
            ;;

            *)
                default_parameters_found=$((default_parameters_found + 1))

                if [ $default_parameter_max -gt -1 ]; then
                    if [ "${#default_parameters[@]}" -gt $default_parameter_max ]; then
                        usage 1 "$default_parameter_max parameters at most can be used...."
                    fi
                fi

                default_parameters=("${default_parameters[@]}" "$1")
                shift 1
            ;;
        esac
    done

     # or is -o
     # ( $default_parameter_max >   -1 and "${#default_parameters[@]}" >   $default_parameter_max )
    if [ $default_parameter_max -gt -1 -a  "${#default_parameters[@]}" -gt $default_parameter_max ]; then
        usage 1 "Up to $default_parameter_max parameters can be informed (${#default_parameters[@]} found)"
    fi

    if [ "${#default_parameters[@]}" -lt $default_parameter_min ]; then
        usage 1 "At least $default_parameter_min parameters must be informed"
    fi

    command_="${default_parameters[@]}"
}

function check_dependencies_availability() {
    for dependency in "${dependencies[@]}"; do
        which "$dependency" &> /dev/null || usage 99 "$dependency command must be available to this script work properly, check your distro packages"
    done
}

function main() {
    parse_command_line "$@"

    check_dependencies_availability

    if [ "$verbose" = "yes" ]; then
        echo "ok_message = '$ok_message'"
        echo "error_message = '$error_message'"
        echo "command = '${command_[@]}'"
        echo "verbose = '$verbose'"
    fi

    tmux new-session -s "$(basename $script_name) $(date +'%Y%m%d-%H%M')" "${command_[@]} && notify-send -t 0 -a \"$job_name\" \"Command done\" \"\$(eval echo '$ok_message')\" || notify-send -t 0 -a \"$job_name\" \"Command done (exit: \$?)\" \"\$(eval echo '$error_message')\""
}


main "$@"

cd "$initial_folder"
