#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import math

__debugging__ = False


def log(message_format, message):
    if __debugging__:
        if message:
            print(message_format.format(message))
        else:
            print(message_format)


def parse_date_param(raw_input):
    try:
        return datetime.datetime.strptime(raw_input, '%d/%m/%Y')
    except ValueError as e:
        if raw_input.lower() == 'today':
            return datetime.datetime.today().date()
        elif raw_input.lower() == 'yesterday':
            return datetime.datetime.today().date() - datetime.timedelta(days=1)
        elif raw_input.lower() == 'tomorrow':
            return datetime.datetime.today().date() + datetime.timedelta(days=1)
        else:
            raise e


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument("start", type=parse_date_param, help="Start date in format: \"DD/MM/YYYY\"")
    parser.add_argument("end", type=parse_date_param, help="End date in format: \"DD/MM/YYYY\"")

    # TODO Add info about possible date formats
    # TODO Add support for date+time params
    # TODO Add support for time only params
    # TODO Support more date formats, like dd/mmm, dd/mm/yyyy, "next sunday", etc 
    # re.match('(?P<base_date>today|yesterday|tomorrow)( *(?P<operand>\+|-) *(?P<value>[0-9]+) *(?P<value_type>days?|weeks?|months?|years?))?', value)

    return parser.parse_args()


def comma_if_some_of_is_nonzero(*values):
    for value in values:
        if value != 0:
            return ", "

    return ""


def main():
    args = parse_command_line()

    log("args: {}", args)

    start = args.start
    end = args.end

    if args.start > args.end:
        temp = start
        start = end
        end = temp

    diff = end - start

    total_days = diff.days + 1

    years = math.floor(total_days / 365)
    total_days -= years * 365

    months = math.floor(total_days / 30)
    total_days -= months * 30

    weeks = math.floor(total_days / 7)
    total_days -= weeks * 7

    days = total_days

    add_s_if_plural = lambda i: "s" if i != 1 else ""

    if years != 0:
        format = "{years} year" + add_s_if_plural(years) + comma_if_some_of_is_nonzero(months, weeks, days)
    else:
        format = ""

    if months != 0:
        format += "{months} month" + add_s_if_plural(months) + comma_if_some_of_is_nonzero(weeks, days)

    if weeks != 0:
        format += "{weeks} week" + add_s_if_plural(weeks) + comma_if_some_of_is_nonzero(days)

    if days != 0:
        format += "{days} day" + add_s_if_plural(weeks)

    data = {'years': years, 'months': months, 'weeks': weeks, 'days': days}

    print(format.format(**data))

if __name__ == '__main__':
    main()
