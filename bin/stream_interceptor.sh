#!/usr/bin/bash

cat /dev/stdin | awk '''
BEGIN {
'"$(for ((i=1; i<=$#; i++)); do sequence=${!i}; echo "    sequences[$i] = \"$sequence\"; "; done)"'
}
{
    print $0;

    for (sequence in sequences) {
        if (index($0, sequences[sequence]) > 0) {
            system("notify-send Interceptor \"" $0 "\"")
        }
    }
}
'''
