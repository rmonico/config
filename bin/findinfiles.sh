#!/bin/sh

# TODO Remake in Python, its becaming very complicated
# Skel variables
script_name="$(basename $0)"
initial_folder="$(pwd)"

# Default optional options values
verbose="no"
file_name=""
file_regex=""

# Required parameters
# set max to any negative number for an arbitrary default parameter count
default_parameter_max=1
default_parameter_min=1
default_parameters=()

# TODO Options from config file
# TODO Log file options -- Dar uma olhada no commando logger, só não encontrei onde ele dá saída (além do stderr)
# TODO If a option is defined more than one time should stop

# usage [ exit_code [ error_message ] ] 
function usage() {
    if [ $# -eq 2 ]; then
        echo
        echo "Error: $2"
    fi

    echo
    echo "$script_name [ -v | --verbose ] [ -h | --help ] [ -f | --file-name <expression> ] [ -r | --file-regex <expression> ] <regex pattern> ..."
    echo
    echo " -f, --file-name      Passed to 'find -name'"
    echo " -r, --regex-pattern  Passed to 'find -regex'"
    echo " regex pattern        <pattern> on 'grep -E <pattern>'"
    echo " -h, --help           This help message"
    echo " -v, --verbose        Enable verbosity"
    echo

    # TODO Examples
    # TODO Return values

    if [ $# -gt 0 ]; then
        exit $1
    fi
}

function parse_command_line() {
    local default_parameters_found=0

    while [ $# -gt 0 ]; do
        case "$1" in
            "-f" | "--file-name")
                file_name="$2"
                shift 2
            ;;

            "-r" | "--regex-pattern")
                file_regex="$2"
                shift 2
            ;;

            "-v" | "--verbose")
                verbose="yes"
                shift 1
            ;;

            "-h" | "--help")
                usage 0
            ;;

            *)
                default_parameters_found=$((default_parameters_found + 1))

                if [ $default_parameter_max -gt -1 ]; then
                    if [ "${#default_parameters[@]}" -gt $default_parameter_max ]; then
                        usage 1 "$default_parameter_max parameters at most can be used...."
                    fi
                fi

                default_parameters=("${default_parameters[@]}" "$1")
                shift 1
            ;;
        esac
    done

     # or is -o
     # ( $default_parameter_max >   -1 and "${#default_parameters[@]}" >   $default_parameter_max )
    if [ $default_parameter_max -gt -1 -a  "${#default_parameters[@]}" -gt $default_parameter_max ]; then
        usage 1 "Up to $default_parameter_max parameters can be informed (${#default_parameters[@]} found)"
    fi

    if [ "${#default_parameters[@]}" -lt $default_parameter_min ]; then
        usage 1 "At least $default_parameter_min parameters must be informed"
    fi

    grep_pattern="${default_parameters[0]}"

    if [ "$file_name" == "" -a "$file_regex" == "" ]; then
        usage 99 "'-f' or '-r' must be specified! (but not both)"
    elif [ "$file_name" != "" -a "$file_regex" != "" ]; then
        usage 98 "'-f' and '-r' can't be used together!"
    fi
}

function main() {
    parse_command_line "$@"

    if [ "$verbose" = "yes" ]; then
        echo "verbose = '$verbose'"
        echo "file_name = '$file_name'"
        echo "file_regex = '$file_regex'"
        echo "grep_pattern = '$grep_pattern'"
    fi

    local find_opts=(-type f -not -path '*/.git*' -not -path '*/target*')

    if [ "$file_name" != "" ]; then
        find_opts=("${find_opts[@]}" -name "$file_name")
    else
        find_opts=("${find_opts[@]}" -regex "$file_regex")
    fi

    OLD_IFS="$IFS"
    IFS='
'

    temp_file=$(mktemp)

    local grep_binary="$(which -a grep | /usr/bin/grep -v 'aliased' | head -n 1)"
    for file in $(find "$(pwd)" "${find_opts[@]}"); do
        $grep_binary -in --color=always -E -- $grep_pattern "$file" > $temp_file && { echo ">>>>> $file"; cat $temp_file; echo "===== $file"; echo; }
    done | less

    rm $temp_file
}


main "$@"

cd "$initial_folder"
