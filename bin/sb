#!/usr/bin/env bash

_verbose="no"

while [ "$#" -gt 0 ]; do
    case "$1" in
        "-h" | "--help" | "h" | "help")
            cat << EOF
Handle a Spring Boot server

Usage: $0 command

Where "command" may be:

start, s     :  Start spring boot
stop, t      :  Stop spring boot
restart, r   :  Restart spring boot
isrunning, i :  Tells if spring boot is running
EOF
            exit 0
        ;;

        "-v" | "--verbose")
            _verbose="yes"
            shift
        ;;

        *)
            echo "$1" | jq . &> /dev/null && _body="$1" || _path="$1"
            shift
        ;;
    esac
done

if [[ -z "$api_host" ]]; then
    echo "'api_host' variable not defined"
    exit 1
fi

if [[ -z "$_path" ]]; then
    echo "'path' not defined in parameters"
    exit 1
fi

log() {
    [[ "$_verbose" == "yes" ]] && echo $@
}

_get_url() {
    local _full_path=$([[ "$_ignore_base_path" == "yes" ]] && echo "${_path}" || echo "${api_base_path}${_path}")

    echo "${api_protocol:-"http"}://${api_host}:${api_port:-8080}${_full_path}"
}

headers_file="$(mktemp -p ${TMPDIR:-/tmp} api_headers.XXXXXXXXXX)"
log "Headers file: '${headers_file}'"

raw_body_file="$(mktemp -p ${TMPDIR:-/tmp} api_raw_body.XXXXXXXXXX)"
log "Raw body file: '${raw_body_file}'"

_url="$(_get_url)"
log "url: '${_url}'"
log "curl options: '${api_curl_options}'"

log "Caling curl...."

[[ -n "$_body" ]] && (body=(-d "$_body"); log "Passing body: '$_body'")

curl ${api_curl_options[@]:-"--silent"} -D "$headers_file" -o "$raw_body_file" "$_url" ${body[@]}

ok=$(head -n 1 "$headers_file" | grep --extended-regexp '^HTTP/[0-9]+.[0-9]+ 2[0-9]{2} ' &> /dev/null && echo "true" || echo "false")

[[ "$ok" == "true" ]] && log "Request OK" || log "Request failed"

cat "$headers_file"

body_file="$(mktemp api_body.XXXXXXXXXX)"
cat "$raw_body_file" | jq -C . > "$body_file" && _is_json="yes" || _is_json="no"

[[ "$_is_json" == "yes" ]] && ( log "Body is a JSON"; cat "$body_file" ) || ( log "Body is not JSON"; cat "$raw_body_file" )

if [ "$_keep_files" == "no" ]; then
    log "Removing request files"
    rm "$headers_file" "$raw_body_file" "$body_file" &> /dev/null
else
    log "Keeping files"
fi

