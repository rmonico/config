def remove_color_codes(input):
    result = str(input)

    while (idx := result.find('\x1b')) > -1:
        end = result.find('m', idx+1)
        if end > 1:
            result = result[:idx] + result[end+1:]
        else:
            return result

    return result
