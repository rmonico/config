import unittest
from clean_color_codes import clean_color_codes

class CleanColorCodesTests(unittest.TestCase):

    def test_given_bytes_with_color_codes_should_remove_them(self):
        self.assertEqual(b'prefix contents suffix', clean_color_codes(b'prefix \x1bcolor_codemcontents\x1banother_codem suffix'))


    def test_given_bytes_with_unclosed_color_codes_should_remove_them(self):
        self.assertEqual(b'contents ', clean_color_codes(b'contents \x1bunclosed color code'))

    def test_given_bytes_with_no_color_codes_should_return_same_bytes(self):
        self.assertEqual(b'contents', clean_color_codes(b'contents'))

    def test_given_bytes_with_just_close_color_code_should_return_same_bytes(self):
        self.assertEqual(b'contents m suffix', clean_color_codes(b'contents m suffix'))

if __name__ == '__main__':
    unittest.main()
