import logging
import sys

_to_logging_verbosity_level = {
    0: 'ERROR',
    1: 'WARNING',
    2: 'INFO',
    3: 'DEBUG',
}


def make_verbosity_argument(parser):
    # TODO Limit choices
    return parser.add_argument('--verbosity',
                               '-v',
                               action='count',
                               default=0, # ERROR + CRITICAL
                               help='Verbosity level (up to 3 v\'s)')


def get_log_level(verbosity):
    if verbosity not in _to_logging_verbosity_level.keys():
        raise Exception(
            f'Invalid verbosity value ("{verbosity}"), pass between 0 and 4')

    return _to_logging_verbosity_level[verbosity]


def configure(verbosity: int):
    level = get_log_level(verbosity)

    logging.basicConfig(level=level)

    logging.getLogger(__name__).debug('Root logger configured to level %s',
                                      level)


# FIXME not working
class conditional(object):
    def __init__(self, wrapped):
        self._wrapped = wrapped
        self.run = False

    def evaluate(self, condition, operator):
        if operator == 'reset':
            self.run = condition
        elif operator == 'or':
            self.run = self.run or condition
        elif operator == 'and':
            self.run = self.run and condition
        else:
            raise Exception(f'Invalid operator: {operator}')

    def when(self, condition):
        self.evaluate(condition, 'reset')
        return self

    def start(self):
        self.run = True
        return self

    def _or(self, condition):
        self.evaluate(condition, 'or')
        return self

    def _and(self, condition):
        self.evaluate(condition, 'and')
        return self

    lock = False

    def __getattr__(self, name):
        breakpoint()

        def wrappedCall(*args, **kwargs):
            return a(self._wrapped, *args, **kwargs)

        def attr():
            a = getattr(self._wrapped, name)

            from types import FunctionType

            if isinstance(a, FunctionType):
                print('in attr, is function')
                return wrappedCall
            else:
                print('in attr, is not function')
                return a

        if conditional.lock:
            result = self.__dict__[name]

            print(
                f'Ignoring lock: {name}, returning: {result}'
            )

            return result

        conditional.lock = True
        print('in getattr, name: ' + name)

        if self.run:
            print('in getattr, run')
            a = attr()
        else:
            print('in getattr, not run')
            a = property(self._wrapped, lambda: None, lambda v: v)

        print('unlocked')
        conditional.lock = False

        return a

    def __setattr__(self, attr, value):
        print('in setattr')
        return setattr(self._wrapped, attr, value)


def get(caller):
    if isinstance(caller, str):
        caller_name = caller
    elif hasattr(caller, 'logger_name'):
        if callable(caller.logger_name):
            caller_name = caller.logger_name()
        else:
            caller_name = str(caller.logger_name)
    else:
        raise Exception(
            '"get" param must be a "str" or have a attribute "logger_name"')

    # logger = conditional(logging.getLogger(caller_name))

    logger = logging.getLogger(caller_name)

    return logger
