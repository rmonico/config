#!/usr/bin/env python3
# coding=UTF-8

from climenu import Menu, MenuItem, ListPickerMenu, ListMultiPickerMenu, input_string, input_integer, input_boolean

def create(main_menu = False):
	menu_title = "xxx"

	menu = Menu(menu_title)

	@menu.before_print
	def before_print(menu):
		print("xxx")
		print()

	@menu.new_item("a", "Label")
	def option(item):
		print("xxx")

		# Optional, default is NONE
		# return MenuItem.NONE
		# return MenuItem.BACK
		# return MenuItem.QUIT

	def pick_single_item(function, *args):
		itens = ["List item 1", "List item 2", "List item 3"]

		picker = ListPickerMenu("Items", itens, "Select a item: ")

		picker.run()

		if picker.selection == MenuItem.ITEM_PICKED:
			item_name = itens[picker.selected_item_index]
			
			function(item_name, *args)
			
			return MenuItem.NONE
		else:
			return picker.selection

	@menu.new_item("b", "Pick a single item from a list")
	def single_item_picker(item):
		def my_function(selected_item_name, extra_args):
			print("Selected item: " + selected_item_name)
			print("Extra args: " + extra_args)

		return pick_single_item(my_function, "arg to my_function")

	def pick_multi_itens(function, *args):
		itens = ["List item 1", "List item 2", "List item 3"]

		picker = ListMultiPickerMenu("Items", itens, "Select a item: ")

		picker.run()

		if picker.selection == MenuItem.ITEM_PICKED:
			selected_item_names = []

			for selected_item in picker.selected_itens:
				selected_item_names.append(itens[selected_item])

			function(selected_item_names, *args)

			return MenuItem.NONE
		else:
			return picker.selection

	@menu.new_item("c", "Pick multiple itens from a list")
	def multi_item_picker(item):
		def my_function(selected_itens, extra_args):
			for item in selected_itens:
				print("Selected item: " + item)

			print("Extra args: " + extra_args)

		return pick_multi_itens(my_function, "arg to my_function")

	# Another menu item, label is run as a commando when choose
	menu.new_os_command("d", "ls -la")

	@menu.new_item("e", "Inputs")
	def inputs(item):
		text = input_string("Prompt: ", "default value")
		confirmation = input_boolean("Prompt [yes/NO]: ", default = False)
		number = input_integer("Prompt: ", default = 21)

	menu.new_submenu("f", "A submenu", Menu("Sub menu"))

	return menu

if __name__ == "__main__":
	create(main_menu = True).run()
