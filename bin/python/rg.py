import documento


def tolist(documento_: str) -> list:
    def digito_toint(d: str) -> int:
        if d in '123456789':
            return int(d)
        elif d == '0':
            return 10
        elif d == 'X':
            return 11
        else:
            return '-'

    return documento.tolist(documento_, digito_toint)


def tostr(documento_: list) -> str:
    def digito_tostr(d: int) -> str:
        if d == 10:
            return '0'
        elif d == 11:
            return 'X'
        else:
            return str(d)

    return documento.tostr(documento_, digito_tostr)


def digito(rg: str) -> int:
    if isinstance(rg, str):
        algarismos = [int(algarismo) for algarismo in rg]
    elif isinstance(rg, list):
        algarismos = rg
    else:
        raise Exception('"rg" parameter must be either str or list')

    digito = 0

    for algarismo, peso in zip(algarismos, range(2, 10)):
        digito += algarismo * peso

    digito = 11 - (digito % 11)

    return digito


def gerar() -> list:
    QUANTIDADE_DIGITOS = 8

    import random

    rg = [random.randrange(10) for i in range(QUANTIDADE_DIGITOS)]

    digito_ = digito(rg)

    rg.append(digito_)

    return rg


def valido(rg: list) -> bool:
    if len(rg) != 9:
        return False

    corpo = rg[0:8]

    digito_informado = rg[8]

    digito_ = digito(corpo)

    return digito_informado == digito_


def pontuar(rg: str) -> str:
    return f'{rg[0:2]}.{rg[2:5]}.{rg[5:8]}-{rg[8:]}'
