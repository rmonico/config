#!/sbin/python3
# coding=UTF-8

# TODO
# - Opção de habilitar desabilitar o <enter> depois de dar cada comando. Quando todas as opções tiverem apenas um caractere e esta opção estiver no "auto" não deve perguntar nada
# - Pedir para o usuário pressionar enter quando der mais de um comando em menos de 1 segundo
# - Mostrar o texto "running" quando um comando não der nenhuma saída mais de 1 segundo depois que foi lançado
# - Permitir valores default para o picker de lista simples e múltipla


import sys
import subprocess
import platform

class MenuItem:
	def __init__(self, shortcut, label, handler, data = None, wait_for_user_input_after = True, lines_before = 0):
		self.shortcut = shortcut
		self.label = label
		self.handler = handler
		self.data = data
		self.wait_for_user_input_after = wait_for_user_input_after
		self.lines_before = lines_before

	NONE = 1
	BACK = 2
	QUIT = 3
	ITEM_PICKED = 4

	def _print_lines_before(self):
		for i in range(0, self.lines_before):
			print()

	def do_print(self):
		self._print_lines_before()

		print("{} -  {}".format(self.shortcut, self.label))


def clear():
    subprocess.call( "cls" if platform.system() == "Windows" else "clear", shell=True)


def wait_for_user_input(message = None):
	input("Press any key to continue..." if not message else message)


class Menu:
	def __init__(self, title, prompt = None, create_back_menu_item = False, clear_after_every_command = True):
		self.title = title
		self.prompt = prompt
		self._create_back_menu_item = create_back_menu_item
		self.clear_after_every_command = clear_after_every_command
		self.itens = []
		self.do_before_print = None
		self.default_menu_itens_created = False

	def before_print(self, function):
		self.do_before_print = function

		return function

	def new_item(self, shortcut, label, handler = None, data = None, wait_for_user_input_after = True):
		if not handler:
			def interceptor(function):
				self.itens.append(MenuItem(shortcut, label, function, data, wait_for_user_input_after = wait_for_user_input_after))

			return interceptor
		
		self.itens.append(MenuItem(shortcut, label, handler, data, wait_for_user_input_after = wait_for_user_input_after))

	def new_os_command(self, shortcut, label, command = None):
		menuitem = MenuItem(shortcut, label, self.os_command_handler, wait_for_user_input_after = True)
		menuitem.command = command if command else label

		self.itens.append(menuitem)

	def os_command_handler(self, item):
		subprocess.call(item.command, shell=True)

	def new_submenu(self, shortcut, label, submenu):
		menuitem = MenuItem(shortcut, label, self.submenu_handler, data = None, wait_for_user_input_after = False, lines_before = 1)
		menuitem.submenu = submenu

		self.itens.append(menuitem)

		return submenu

	def submenu_handler(self, item):
		action = MenuItem.NONE

		while action == MenuItem.NONE:
			action = item.submenu.run()

			if not action:
				action = MenuItem.NONE

		if action == MenuItem.BACK:
			return MenuItem.NONE
		else:
			return action

	def create_redraw_menu_item(self):
		self.itens.insert(0, MenuItem(".", "Redraw", self.redraw_handler, data = None, wait_for_user_input_after = False, lines_before = 0))

	def redraw_handler(self, item):
		pass

	def create_back_menu_item(self, label = None):
		if self._create_back_menu_item:
			self.itens.append(MenuItem("x", label if label else "Back to previous menu", self.back_handler, data = None, wait_for_user_input_after = False, lines_before = 1))

	def back_handler(self, item):
		return MenuItem.BACK

	def create_quit_menu_item(self):
		self.itens.append(MenuItem("q", "quit", self.quit_handler, data = None, wait_for_user_input_after = False, lines_before = 2))

	def quit_handler(self, item):
		return MenuItem.QUIT

	def _print_menu(self):
		if self.clear_after_every_command:
			clear()

		if self.do_before_print:
			self.do_before_print(self)
			print()

		print("*** {} ***".format(self.title))
		print()

		for item in self.itens:
			item.do_print()

		if self.prompt:
			print()
			sys.stdout.write(self.prompt)

	def create_default_menu_itens(self):
		self.create_redraw_menu_item()
		self.create_back_menu_item()
		self.create_quit_menu_item()

	def run(self):
		if not self.default_menu_itens_created:
			self.create_default_menu_itens()
			self.default_menu_itens_created = True

		action = MenuItem.NONE

		while action == MenuItem.NONE:
			self._print_menu()

			selection = sys.stdin.readline()[:-1]

			selected = None

			for item in self.itens:
				if item.shortcut == selection:
					selected = item
					break

			if selected:
				# TODO Extrair esse if para um método externo para chamar com um item arbitrário de menu, útil para rodar os pickers de lista
				action = selected.handler(item)

				if not action:
					action = MenuItem.NONE

				if action == MenuItem.NONE and selected.wait_for_user_input_after:
					wait_for_user_input()
			else:
				print("Invalid input...")

		return action


class AbstractListPickerMenu:
	def __init__(self, title, labels, prompt = None):
		self.menu = Menu(title, prompt)
		self.menu.create_default_menu_itens = self.create_default_menu_itens
		self.labels = labels

	def run(self):
		selection = self.menu.run()

		if selection == MenuItem.BACK:
			return MenuItem.NONE
		else:
			return selection


class ListPickerMenu(AbstractListPickerMenu):
	def create_default_menu_itens(self):
		self.menu.create_back_menu_item("Cancel and back to previous menu")
		self.menu.create_quit_menu_item()

	def run(self):
		for i in range(len(self.labels)):
			self.menu.new_item(str(i + 1), self.labels[i], self._handler, data = i)

		self.selected_item_index = None

		self.selection = AbstractListPickerMenu.run(self)

		return self.selection

	def _handler(self, item):
		self.selected_item_index = item.data

		return MenuItem.ITEM_PICKED


class ListMultiPickerMenuItem(MenuItem):
	def __init__(self, shortcut, label, data):
		MenuItem.__init__(self, shortcut, label, self.handler, data, wait_for_user_input_after = False)
		self.selected = False

	def do_print(self):
		self._print_lines_before()

		print("[ {} ] {} -  {}".format( "X" if self.selected else " ", self.shortcut, self.label))

	def handler(self, item):
		self.selected = not self.selected


class ListMultiPickerMenu(AbstractListPickerMenu):
	def create_default_menu_itens(self):
		self.menu.create_back_menu_item("Cancel and back to previous menu")
		self.menu.new_item("c", "Clear selected itens", self.clear_selection, wait_for_user_input_after = False)
		self.menu.new_item("a", "Selected all itens", self.select_all_itens, wait_for_user_input_after = False)
		self.menu.new_item("d", "Done", self.selection_done, wait_for_user_input_after = False)
		self.menu.create_quit_menu_item()

	def run(self):
		self.pickable_itens = []

		for i in range(len(self.labels)):
			menu_item = ListMultiPickerMenuItem(str(i + 1), self.labels[i], data = i)
			self.pickable_itens.append(menu_item)
			self.menu.itens.append(menu_item)

		self.selection = AbstractListPickerMenu.run(self)

		if self.selection == MenuItem.ITEM_PICKED:
			self.selected_itens = []

			for item in self.pickable_itens:
				if item.selected:
					self.selected_itens.append(item.data)
		else:
			self.selected_itens = None

		return self.selection

	def clear_selection(self, item):
		self._set_all_itens(False)

	def select_all_itens(self, item):
		self._set_all_itens(True)

	def _set_all_itens(self, selection_state):
		for item in self.pickable_itens:
			item.selected = selection_state

	def selection_done(self, item):
		return MenuItem.ITEM_PICKED


def input_string(prompt, default = None):
	value = input(prompt)

	return value if value else default


def input_integer(prompt, default = None):
	while True:
		value = input(prompt)

		if not value:
			return default

		try:
			int_value = int(value)

			return int_value
		except:
			print("Invalid input. Type a integer number.")
			pass


def input_boolean(prompt, default = None, yes_strings = ["yes", "y"], no_strings = ["no", "n"]):
	while True:
		value = input(prompt)

		if not value:
			if default is not None:
				return default

		if value.lower() in yes_strings:
			return True

		if value.lower() in no_strings:
			return False
		else:
			print("Type '{}' or '{}'.".format(yes_strings[0], no_strings[0]))

