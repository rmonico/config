import os


def existingFilename(filename: str, parser=None):
    if not os.path.isfile(filename):
        parser.error(f'File "{filename}" doesn\'t exit')

    return filename


def existingFilenames(files: list, parser=None):
    for file in files:
        existingFilename(file, parser)

    return files


# TODO Testar
def configDirFor(programName: str = None) -> str:
    from os import environ as env
    from os.path import join

    xdgDir = 'XDG_CONFIG_DIR'
    homeConfigs = join(env['HOME'], '.config')
    configsDir = env.get(xdgDir, homeConfigs)

    return configsDir if not programName else join(configsDir,
                                                   f'.{programName}')


def configFileFor(programName: str = None, filename: str = 'config') -> str:
    from os.path import join

    return join(configDirFor(programName), filename)
