import logger_wrapper

global _logger
_logger = logger_wrapper.get(__name__)


class ArgumentParserBuilder(object):
    def __init__(self, parser):
        self._last_parser = parser
        self._last_subparsers = None
        self._parse_stack = []

    def __enter__(self, *args):
        global _logger
        _logger.debug(f'__enter__: {args}')

        self._parse_stack.append((self._last_parser, self._last_subparsers))

        return self

    def __exit__(self, *args):
        global _logger
        _logger.debug(f'__exit__: {args}')
        self._parse_stack.pop()

    def add_argument(self, *args, **kwargs):
        parser = self._parse_stack[-1][0]

        parser.add_argument(*args, **kwargs)

    def _get_parent_parsers(self):
        parser, subparsers = self._parse_stack[-1]

        if not subparsers:
            subparsers = parser.add_subparsers(dest='command',
                                               required=True,
                                               help='Command to run')
            self._parse_stack[-1] = (parser, subparsers)

        return parser, subparsers

    def add_command(self, command_name, callback=None, *args, **kwargs):
        global _logger
        _logger.info(f'Adding command: {command_name}')

        parent_parser, parent_subparsers = self._get_parent_parsers()

        parser = parent_subparsers.add_parser(command_name, *args, **kwargs)

        parent_command = parent_parser.get_default('command')

        if parent_command:
            command = parent_command + ' '
        else:
            command = ''

        command += command_name

        defaults = {
            'command': command,
            'callback': callback,
        }

        parser.set_defaults(**defaults)

        self._last_parser = parser
        self._last_subparsers = None

        return self


# Exemplo de uso:
# def parse_command_line():
#     parser = argparse.ArgumentParser()
#     parser.add_argument('--verbosity', '-v', action='count', default=0, help='Verbosity level (up to 4 v\'s)')

#     with ArgumentParserBuilder(parser) as b:
#         with b.add_command('note'):
#             b.add_command('activate')
#             b.add_command('hide')
#             b.add_command('toggle')
#             b.add_command('new')

#         with b.add_command('group'):
#             b.add_command('new')
#             with b.add_command('change'):
#                 # TODO Move 'rofi' option as a flag here
#                 b.add_argument('group_name')
#             b.add_command('list')

#         with b.add_command('manager'):
#             b.add_command('open')

#         with b.add_command('settings'):
#             b.add_command('open')

#         with b.add_command('shortcuts'):
#             b.add_command('open')

#         b.add_command('quit')

#         b.add_command('rofi')

#     return parser.parse_args()
# Versão antiga, acho que não está funcionando
# class ArgumentParserBuilder(object):
#     def __init__(self, parser):
#         self._parsers = [(parser, None)]

#     def __enter__(self, *args):
#         parser = self._parsers[-1][0]
#         subparsers = parser.add_subparsers(dest='command', required=True, help='Command to run')

#         self._parsers[-1] = (parser, subparsers)

#         self._parsers.append((None, None))

#         return self

#     def __exit__(self, *args):
#         self._parsers.pop()
#         pass

#     def add_command(self, command_name, callback=None):
#         parent_subparsers = self._parsers[-2][1]

#         parser = parent_subparsers.add_parser(command_name)

#         if callback:
#             parser.set_defaults(callback=callback)

#         self._parsers[-1] = (parser, None)

#         return self
