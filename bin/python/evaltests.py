import sys

stdout = sys.stdout
from unittest import TestCase, main


def write(value=''):
    stdout.write(f'{value}\n')


def true():
    write('in true')
    return 1


def false():
    write('in false')
    return 0


allValues = [(false, false), (true, false), (false, true), (true, true)]


def expected_(operator, op1, op2):
    if operator == 'and':
        return 1 if op1 == true and op2 == true else 0
    elif operator == 'or':
        return 1 if op1 == true or op2 == true else 0
    else:
        raise Exception(
            f'Invalid operator {operator}. Only "and" and "or" are allowed')


class EvalTests(TestCase):
    def testAll(self):
        for operator in ['and', 'or']:
            for op1, op2 in allValues:
                expected = expected_(operator, op1, op2)

                write(
                    f'operator: {operator}, expected: {expected}, op1: {op1.__name__}, op2: {op2.__name__}'
                )
                assertion = self.assertTrue if expected else self.assertFalse
                if operator == 'and':
                    returned = op1() and op2()
                else:
                    returned = op1() or op2()

                write(f'returned: {returned}')

                breakpoint()
                assertion(returned == expected,
                          f'{op1.__name__} {operator} {op2.__name__}')


def runTests():
    main(EvalTests())


if __name__ == '__main__':
    runTests()
