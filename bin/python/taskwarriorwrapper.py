#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess
import logging


logger = logging.getLogger(__name__)
taskwarrior_binary = shutil.which('task')


class TaskwarriorWrapper(object):

    def __init__(self, task_data=None):
        # TODO Its possible to improve this with more specific events
        self._listeners = {'data changed': []}

        if task_data:
            self._environment = os.environ.copy()
            self._environment['TASKDATA'] = task_data
        else:
            self._environment = None

    def _do_run(self, args, clean_stdouterr=False):
        stdout = subprocess.PIPE if clean_stdouterr else None
        stderr = subprocess.PIPE if clean_stdouterr else None

        cmdline = [taskwarrior_binary]

        if clean_stdouterr:
            cmdline.append('rc.defaultwidth:')
            cmdline.append('rc._forcecolor:off')
            cmdline.append('rc.color:off')

        cmdline.extend(args)

        logger.debug('Running ' + '"{}"'.format(item) for item in cmdline)

        process = subprocess.run(cmdline, env=self._environment, stdout=stdout, stderr=stderr)

        return process

    def _run(self, args):
        return self._do_run(args)

    def _run_for_parse(self, args):
        return self._do_run(args, clean_stdouterr = True)


    def register_listener(self, event, listener):
        self._listeners[event].append(listener)

    def remove_listener(self, event, listener):
        self._listeners[event].delete(listener)

    def _notify_listeners(self, event, *args, **kwargs):
        for listener in self._listeners[event]:
            listener(origin=self, *args, **kwargs)

    def get_contexts(self):
        process = self._run_for_parse(['context', 'list'])

        stdout = process.stdout.decode().split('\n')

        contexts = []

        for line in stdout[3:-2]:
            fields = line.split(' ')

            contexts.append(fields[0])

        return contexts


    def set_active_context(self, context):
        self._run(['context', context])

    def get_active_context(self):
        process = self._run_for_parse(['context', 'list'])

        stdout = process.stdout.decode().split('\n')

        for line in stdout:
            fields = line.split(' ')

            if fields[-1] == 'yes':
                return fields[0]

        return None


    def get_reports(self):
        process = self._run_for_parse(['reports'])

        out = process.stdout.decode().splitlines()

        reports = list()
        descriptions = list()

        description_column_start = out[2].find(' ') + 1

        for line in out[3:-2]:
            name = self._get_report_name(line)

            reports.append(name)
            descriptions.append(self._get_report_description(line, description_column_start))

        return zip(reports, descriptions)

    def _get_report_name(self, line):
        start = 0
        end = line.find(' ')

        return line[start:end]

    def _get_report_description(self, line, column_start):
        return line[column_start:]


    def load(self, report, filters, context=None):
        params = list()

        if context:
            params.append('rc.context:{}'.format(context))

        if report:
            params.append(report)

        if filters:
            params.extends(filters)

        process = self._run_for_parse(params)

        return process.stdout.decode('utf-8')

    def add(self, parameters):
        self._run(['add'] + parameters)

        self._notify_listeners('data changed')

    def annotate(self, id, annotation):
        self._run([str(id), 'annotate'] + annotation)

        self._notify_listeners('data changed')

    def done(self, ids):
        self._run([str(i) for i in ids] + ['done'])

        self._notify_listeners('data changed')

    def view(self, ids):
        self._run([str(i) for i in ids])

    def mod(self, ids, modifications):
        self._run([str(i) for i in ids] + ['mod'] + modifications)

        self._notify_listeners('data changed')

    def delete(self, ids):
        self._run([str(i) for i in ids] + ['del'])

        self._notify_listeners('data changed')

    def undo(self):
        self._run(['undo'])

        self._notify_listeners('data changed')

    def sync(self):
        self._run(['sync'])

        self._notify_listeners('data changed')

    def invalidate_data(self):
        self._notify_listeners('data changed')
