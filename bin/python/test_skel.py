#!/usr/bin/env python3
import unittest


class SkelTests(unittest.TestCase):
    """
    run with:
    python -m unittest <this file name with extension>

    should be run from file folder
    """

    def test_tests(self):
        self.assertTrue(True)

        class CustomException(Exception):
            pass

        with self.assertRaises(CustomException):
            raise CustomException()