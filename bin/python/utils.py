import os


def config_file(application: str, configFile: str = 'config') -> str:
    return os.path.join(xdg_folder(application), configFile)


def xdg_folder(_prog_name = None):
    global_xdg_folder = os.environ.get('XDG_CONFIG', os.path.join(os.environ['HOME'], '.config'))

    return os.path.join(global_xdg_folder, _prog_name or prog_name())


def prog_name():
    import sys
    # TODO Acho que só vai funcionar quando estiver rodando como script
    fullpath = sys.argv[0]

    return os.path.basename(fullpath)


def open_in_editor(filename):
    import os
    editor = os.environ.get('EDITOR', '/usr/bin/nvim')

    import subprocess
    subprocess.run([editor, filename])

