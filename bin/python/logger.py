import logging
import sys


_to_logging_verbosity_level = {
    0: "CRITICAL",
    1: "ERROR",
    2: "WARNING",
    3: "INFO",
    4: "DEBUG",
}


def make_verbosity_argument(parser):
    # TODO Limit choices
    return parser.add_argument('--verbosity', '-v', action='count', default=0, help='Verbosity level (up to 4 v\'s)')


def get_log_level(verbosity):
    if verbosity not in _to_logging_verbosity_level.keys():
        raise Exception(f'Invalid verbosity value ("{verbosity}"), pass between 0 and 4')

    return _to_logging_verbosity_level[verbosity]


def configure(args):
    level = get_log_level(args.verbosity)

    logging.basicConfig(level=level)

    logging.getLogger(__name__).debug('Root logger configured to level %s', level)


def get(caller):
    if isinstance(caller, str):
        caller_name = caller
    elif hasattr(caller, 'logger_name'):
        if callable(caller.logger_name):
            caller_name = caller.logger_name()
        else:
            caller_name = str(caller.logger_name)
    else:
        raise Exception('"get" param must be a "str" or have a attribute "logger_name"')

    logger = logging.getLogger(caller_name)

    return logger
