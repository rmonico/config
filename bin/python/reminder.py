#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logger_wrapper
from datetime import datetime
import sys

_DEBUG = False


class InvalidTime(Exception):

    def __init__(self):
        super().__init__('Time format invalid!')


def _is_absolute_time(arg):
    import re
    return re.match('^[0-9]{2}:[0-9]{2}$', arg) != None


def _relative_time_in_minutes(arg):
    import re

    m = re.match(r'^([0-9]+)(?: )?(min(?:ute)?|hour)(?:s)?$', arg)

    if not m:
        return False

    time_ammount = m.groups()[0]
    time_unit = m.groups()[1]

    if time_unit in ['min', 'minute']:
        return int(time_ammount)
    elif time_unit == 'hour':
        return int(time_ammount) * 60
    else:
        assert False


def remaining_time_in_seconds(arg):
    current = datetime.now()

    if _is_absolute_time(arg):
        thetime = datetime.strptime(arg, '%H:%M')

        diff = thetime - current

        return diff.seconds
    elif minutes := _relative_time_in_minutes(arg):
        return minutes * 60
    else:
        raise InvalidTime()


_FOREGROUND_SWITCH = '--foreground'


def _parse_command_line(args):
    '''
    Reference: https://docs.python.org/3/library/argparse.html
    '''
    parser = argparse.ArgumentParser(
        description='Show a message on screen after some time')

    # lembrar 10min 'Dar um tempo'
    # lembrar 20:00 'Tomar remédio'

    parser.add_argument('--title',
                        '-t',
                        default='Reminder',
                        help='Title of message')
    parser.add_argument('when',
                        type=remaining_time_in_seconds,
                        help='When the message must be shown')
    parser.add_argument(
        _FOREGROUND_SWITCH,
        action='store_true',
        help='Wait in foreground instead create a tmux session')
    parser.add_argument('message', help='The message to be shown')

    logger_wrapper.make_verbosity_argument(parser)

    return parser.parse_args()


def error(message, exit_code=1):
    global logger
    logger.critical(message)
    sys.exit(exit_code)


def wait(when):
    import time

    if not _DEBUG:
        time.sleep(when)


# TODO Read from file
MESSAGE_COMMAND = ['notify-send', '{title}', '{message}']


def show_message(**kwargs):
    command = [c.format(**kwargs) for c in MESSAGE_COMMAND]

    global logger
    logger.debug('Running ' + ' '.join([f'"{param}"' for param in command]))

    import subprocess
    p = subprocess.run(command)

    return p.returncode


def _recall_itself_inside_tmux(**kwargs):
    import sys

    reminder_args = list(sys.argv)
    if _FOREGROUND_SWITCH not in reminder_args:
        reminder_args.insert(1, _FOREGROUND_SWITCH)

    import shlex
    reminder_command = ' '.join(shlex.quote(arg) for arg in reminder_args)

    command = [
        'tmux', 'new-session', '-d', '-s',
        '{title}_{message}'.format(**kwargs), reminder_command
    ]

    global logger
    logger.debug('Running ' + ' '.join([f'"{param}"' for param in command]))

    import subprocess
    p = subprocess.run(command)

    return p.returncode


def make(when, message, title='Reminder'):
    logger.debug('Will wait %d seconds', when)
    logger.debug('Will display message "%s"', message)

    wait(when)

    return show_message(title=title, message=message)


def _main(when, message, title='Reminder', foreground=False, verbosity=0):
    if foreground:
        return make(when, message, title)

    returncode = _recall_itself_inside_tmux(when=when,
                                            message=message,
                                            title=title,
                                            verbosity=verbosity)

    if returncode == 0:
        print('Reminder registered in tmux')
    else:
        print(f'Error registering reminder in tmux (code {returncode})')

    return returncode


def main(*args, **kwargs):
    cli_args = list()

    for a in args:
        # O que fazer com None?
        cli_args.append(str(a))

    for key, value in kwargs.items():
        flag = isinstance(value, bool)

        if not flag:
            cli_args.append(f'--{key}')
            cli_args.append(str(value))
        else:
            if value:
                cli_args.append(f'--{key}')

    return entrypoint(cli_args)


def entrypoint(_args=sys.argv[1:]):
    args = _parse_command_line(_args)

    logger_wrapper.configure(args.verbosity)
    '''
    Logger reference: https://docs.python.org/3/library/logging.html
    '''
    global logger
    logger = logger_wrapper.get(__name__)

    return _main(**args.__dict__)


if __name__ == '__main__':
    returncode = entrypoint() or 0

    logger.warning(f'Exiting with status {returncode}')

    sys.exit(returncode)
