import unittest
import mock
from reminder import *
import datetime

mocked_datetime = mock.patch('reminder.datetime').start()

mocked_datetime.now.return_value = datetime.datetime(2010, 1, 1, 0, 0)
mocked_datetime.side_effect = lambda *args, **kw: datetime.datetime(
    *args, **kw)


class RemainingTimeInSecondsTests(unittest.TestCase):
    def test_should_raise_error_on_invalid_time(self):
        self.assertRaises(InvalidTime, remaining_time_in_seconds, 'xxxx')

    def test_should_parse_absolute_time(self):
        result = remaining_time_in_seconds('20:00')

        self.assertEqual(result, 20 * 3600, 'Absolute remaining time')

    def test_should_parse_relative_time_in_minutes(self):
        result = remaining_time_in_seconds('10min')

        self.assertEqual(result, 10 * 60, 'Relative remaining time in minutes')

    def test_should_parse_relative_time_in_hours(self):
        result = remaining_time_in_seconds('3hours')

        self.assertEqual(result, 3 * 3600, 'Relative remaining time in hours')

    def test_should_parse_relative_hours_in_singular(self):
        result = remaining_time_in_seconds('1hour')

        self.assertEqual(result, 3600, 'Relative remaining time in hours')

    def test_should_parse_relative_minutes_in_plural(self):
        result = remaining_time_in_seconds('30mins')

        self.assertEqual(result, 1800, 'Relative remaining time in hours')

    def test_should_parse_relative_time_with_space(self):
        result = remaining_time_in_seconds('30 mins')

        self.assertEqual(result, 1800, 'Relative remaining time in hours')

    def test_should_parse_relative_minutes(self):
        result = remaining_time_in_seconds('30 minutes')

        self.assertEqual(result, 1800, 'Relative remaining time in hours')


# TODO 1hour (no singular)
# TODO 1 hour (com espaço)

mocked_datetime.close()
