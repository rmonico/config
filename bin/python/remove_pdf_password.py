#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logger_wrapper
import subprocess


def remove_pdf_password(password: str, filename: str, verbosity: int = 1):
    logger_wrapper.configure(verbosity)
    '''
    Logger reference: https://docs.python.org/3/library/logging.html
    '''
    global logger
    logger = logger_wrapper.get(__name__)

    qpdfParams = [
        'qpdf', f'--password={password}', '--decrypt', '--replace-input',
        filename
    ]
    logger.info(f'Calling {qpdfParams}')

    process = subprocess.run(qpdfParams)

    return process
