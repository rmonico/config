#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logger_wrapper
from sys import exit, stdin, stdout


def custom_argument_type(raw):
    return 'returned parameters'


def parse_command_line():
    '''
    Reference: https://docs.python.org/3/library/argparse.html
    '''
    from argparse import ArgumentParser, FileType

    parser = ArgumentParser(description='Help message')

    parser.add_argument('required',
                        help='Required argument')

    parser.add_argument('-o',
                        '--optional',
                        help='Optional argument')

    parser.add_argument('custom-type',
                        type=custom_argument_type,
                        help='Argument with custom type')

    parser.add_argument('infile',
                        nargs='?',
                        type=FileType('r'),
                        default=stdin,
                        help='Input file, default to stdin')

    parser.add_argument('outfile',
                        nargs='?',
                        type=FileType('w'),
                        default=stdout,
                        help='Output file, defaults to stdout')

    subparsers = parser.add_subparsers()

    add_parser = subparsers.add_parser('add', help='Add sub command')
    add_parser.add_argument('same as parser')

    # TODO argparsebuilder

    logger_wrapper.make_verbosity_argument(parser)

    return parser.parse_args()


def error(message, exit_code=1):
    global logger

    logger.critical(message)

    exit(exit_code)


def main():
    global args

    args = parse_command_line()

    logger_wrapper.configure(args.verbosity)
    '''
    Logger reference: https://docs.python.org/3/library/logging.html
    '''
    global logger
    logger = logger_wrapper.get(__name__)

    return 0


if __name__ == '__main__':
    returncode = main() or 0

    logger.warning(f'Exiting with status {returncode}')

    exit(returncode)
