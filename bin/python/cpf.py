from documento import tolist, tostr


def digitos(cpf: list) -> list:
    digito = 0

    for algarismo, peso in zip(cpf, range(10, 1, -1)):
        digito += algarismo * peso

    digito1 = digito % 11

    digito1 = 0 if digito1 < 2 else 11 - digito1

    digito = 0
    for algarismo, peso in zip(cpf + [digito1], range(11, 1, -1)):
        digito += algarismo

    digito2 = digito % 11

    digito2 = 0 if digito2 < 2 else 11 - digito2

    return [digito1, digito2]


def gerar() -> list:
    QUANTIDADE_DIGITOS = 9

    import random

    corpo = [random.randrange(10) for i in range(QUANTIDADE_DIGITOS)]

    digitos_ = digitos(corpo)

    return corpo + digitos_


def valido(cpf: list) -> bool:
    if len(cpf) != 11:
        return False

    corpo = cpf[0:9]

    digitos_informado = cpf[9:11]

    digitos_ = digitos(corpo)

    return digitos_informado == digitos_


def pontuar(cpf: str) -> str:
    return f'{cpf[0:3]}.{cpf[3:6]}.{cpf[6:9]}-{cpf[9:11]}'
