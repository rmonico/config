#!/usr/bin/env python3
import unittest
from types import SimpleNamespace


class Optional(object):
    """

    Usages:
    Optional(ref).of('attribute.subattr.etc').get()
    # Return None if:
        ref is None
        ref.attribute doesnt exist or is None
        ref.attribute.subattr doesnt exist or is None
        ref.attribute.subattr.etc doesnt exist
      else return ref.attribute.subattr.etc

    Optional(ref).get(default=value)
    Optional(ref).get('default value')
    Optional(ref).get(orRaise=exception_object)
    Optional(ref).of('attribute.subattr').of('method_of_subattr()', (arg), {kwargs}).get()
    Optional(ref).of('method()', (optional_args), {optional_kwargs}).get()
    Optional(ref).of('method().unpacked_from_methods_return').get()
    Optional(ref).of('attr.method()').get()
    Optional(ref).of('attribute.subattr').map(callable).get()
    # In the above case callable will receive subattr's value as parameter. callable's returned value (even if None) will be packed in another Optional
    """

    _statics_initialized = False
    NONE_OPTIONAL = 'see _init_statics'

    def __init__(self, reference):
        self._ref = reference

        self._init_statics()

    def _init_statics(self):
        if Optional._statics_initialized:
            return

        Optional._statics_initialized = True

        Optional.NONE_OPTIONAL = Optional(None)

    def get(self, default: object = None, orRaise: Exception = None) -> object:
        if default is not None and orRaise is not None:
            raise ValueError('Either "default" nor "orRaise" must be specified')

        if self._ref is not None:
            return self._ref

        if orRaise is not None:
            raise orRaise

        return default

    # TODO isNone() -> bool:

    def of(self, attribute_chain: str):
        if self._ref is None:
            return Optional.NONE_OPTIONAL

        attribute = self._ref

        for attribute_name in attribute_chain.split('.'):
            if not hasattr(attribute, attribute_name):
                return Optional.NONE_OPTIONAL

            attribute = getattr(attribute, attribute_name)

        return Optional(attribute)


class OptionalGetTests(unittest.TestCase):

    def test_optional_with_none_reference_should_return_none(self):
        self.assertIsNone(Optional(None).get())

    def test_optional_with_valid_reference_should_return_it(self):
        self.assertEqual('value', Optional('value').get())


class OptionalGetWithDefaultTests(unittest.TestCase):

    def test_optional_with_none_reference_and_default_should_return_the_default(self):
        self.assertEqual('value', Optional(None).get('value'))

    def test_optional_with_valid_reference_and_default_should_return_the_reference(self):
        self.assertEqual('reference', Optional('reference').get('value'))


class CustomException(Exception):
    pass


class OptionalGetWithOrRaiseTests(unittest.TestCase):

    def test_optional_with_none_reference_and_orRaise_should_raise_exception(self):
        self.assertRaises(CustomException, lambda: Optional(None).get(orRaise=CustomException()))

    def test_optional_with_valid_reference_and_orRaise_should_return_reference(self):
        self.assertEqual('value', Optional('value').get(orRaise=CustomException()))


class OptionalGetWithDefault OrRaiseAndDefaultTests(unittest.TestCase):

    def test_optional_with_none_reference_and_orRaise_and_default_should_raise_internal_exception(self):
        self.assertRaises(ValueError, lambda: Optional(None).get(default='value', orRaise=CustomException()))

    def test_optional_with_valid_reference_and_orRaise_and_default_should_return_reference(self):
        self.assertRaises(ValueError, lambda: Optional('value').get(default='default', orRaise=CustomException()))


# class BaseOptionalTests(unittest.TestCase):

#     def _make_fixtures(self):
#         return {
#             reference: {
#                 none: lambda fixture: Optional(None),
#                 valid: lambda fixture: Optional('value'),
#             }

#             default: {
#                 without: lambda fixture: fixture.get(),
#                 valid: lambda fixture: fixture.get('default'),
#             }

#             orRaise: {
#                 without: lambda fixture: fixture.get(),
#             }
#         }

#     def setUp(self):
#         self._contexts = self._make_fixtures()

class OptionalAttributeUnpackTests(unittest.TestCase):

    def test_optional_reference_with_valid_attribute_should_return_the_attribute_value(self):
        self.assertEqual('value', Optional(SimpleNamespace(attr='value')).of('attr').get())

    def test_optional_with_none_reference_and_unexisting_attribute_should_return_none(self):
        self.assertIsNone(Optional(None).of('attr').get())

    def test_optional_reference_and_unexisting_attribute_should_return_none(self):
        self.assertIsNone(Optional(object()).of('attr').get())

    def test_optional_reference_and_unexisting_attribute_and_default_should_the_default(self):
        self.assertEqual('default', Optional(object()).of('attr').get('default'))

    def test_optional_reference_and_unexisting_attribute_and_orraise_should_raise_exception(self):
        self.assertRaises(CustomException, lambda: Optional(object()).of('attr').get(orRaise=CustomException))

    def test_optional_reference_and_unexisting_attribute_and_orraise_should_raise_exception(self):
        self.assertRaises(ValueError, lambda: Optional(object()).of('attr').get('default', orRaise=CustomException))

    def test_optional_reference_with_none_attribute_should_return_none(self):
        self.assertIsNone(Optional(SimpleNamespace(attr=None)).of('attr').get())
        # TODO Test with default
        # TODO Test with orRaise
        # TODO Test with default and orRaise

class OptionalNestedAttributeUnpackTests(unittest.TestCase):

    def test_optional_reference_with_valid_nested_attribute_should_return_the_attribute_value(self):
        self.assertEqual('value', Optional(SimpleNamespace(attr=SimpleNamespace(subattr='value'))).of('attr.subattr').get())
        # TODO Test with default
        # TODO Test with orRaise
        # TODO Test with default and orRaise
