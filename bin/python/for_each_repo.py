#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import subprocess
import sys


def log(level, message, *args_):
    if args.verbose >= level:
        print(message.format(*args_))


def log_dev(message, *args_):
    log(3, message, *args_)


def log_warning(message, *args_):
    log(1, message, *args_)


def parse_command_line():
    parser = argparse.ArgumentParser(
        prog='for_each_repo', description='Run an arbitrary command on every repository under current folder')

    parser.add_argument('-v', '--verbose', action="count",
                        default=0, help="Increases script verbosity")

    parser.add_argument('--todo', action="store_true",
                        help="Show development todo list")

    parser.add_argument('-fr', '--fake-run', action="store_true",
                        help="Doesn't anything, just simulate what would do")

    parser.add_argument('-mi', '--make-index', action="store_true",
                        help="Make a repository index file here and exit")

    parser.add_argument('-ri', '--repository-index',
                        help="Change the default repository index file (default: .repos)", default=".repos")

    parser.add_argument('command', nargs=argparse.REMAINDER)

    args = parser.parse_args()

    if args.command == [] and not args.make_index and not args.todo:
        print("Error: 'command' must be informed")
        print("")

        parser.print_usage()

        sys.exit(2)

    return args


def _do_list_repositories(folder):
    repositories = []

    for directory in os.scandir(folder):
        if directory.is_dir():
            directory_path = os.path.join(folder, directory)

            if os.path.exists(os.path.join(folder, directory, '.git')):
                repositories.append(directory_path)
            else:
                for entry in _do_list_repositories(directory_path):
                    repositories.append(entry)

    return repositories


def list_repositories():
    return _do_list_repositories(os.getcwd())


def make_index():
    repository_list = list_repositories()

    repository_index_file = open(args.repository_index, 'wt')

    for repository in repository_list:
        repository_index_file.write(repository + "\n")

    repository_index_file.close()


def load_repositories():
    repositories = []

    repository_index_file = open(args.repository_index, 'rt')

    line = repository_index_file.readline()

    while line:
        if len(line) > 0 and line[0] != "#":
            repositories.append(line[:-1])

        line = repository_index_file.readline()

    return repositories


def run_command_on_repository(repository_path):
    formatted_command = []

    for command in args.command:
        repository_name = os.path.basename(repository_path)

        formatted_command.append(command.format(repository_name, repository_path))

    friendly_command = " ".join(formatted_command)

    print()
    print(">>> Running '{}' on folder '{}'...".format(friendly_command, repository_path))

    if not args.fake_run:
        os.chdir(repository_path)

        sys.stdout.flush()
        subprocess.run(formatted_command)

    print()

def run_command_for_repositories():
    repositories = load_repositories()

    for repository in repositories:
        run_command_on_repository(repository)


def print_todo():
    print("- Salvar a saída em arquivo externo")
    print("- Salvar o código de retorno (quando tiver erro) e stderr em arquivos na pasta atual")
    print("- Guardar paths relativos à pasta atual no índice")
    print("- Mostrar o progresso na saída")
    print("- Está mostrando a saída dos comandos antes do print informando em qual repositório está rodando")
    print("- Fazer notificação quando terminar de rodar o comando")
    print("- Confirmar quando todos os comandos forem executados sem problema (ou informar quando tiver problema)")


def main():
    global args

    args = parse_command_line()

    log_dev('Command: {}', args.command)
    log_dev('Make index: {}', args.make_index)
    log_dev('Repository index file: {}', args.repository_index)

    if args.todo:
        print_todo()

        return

    if args.make_index:
        make_index()

        print("Index made sucessfully and save to {}".format(args.repository_index))

        return

    run_command_for_repositories()


if __name__ == "__main__":
    main()
