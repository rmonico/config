import re
import ipdb


COLOR_CODE_START = b'\x1b'
COLOR_CODE_END = b'm'

def clean_color_codes(raw):
    # import ipdb; ipdb.set_trace()
    clean = b''

    start = 0
    finished = False
    while not finished:
        end = raw.find(COLOR_CODE_START, start)
        if end == -1:
            end = len(raw)
            finished = True

        clean += raw[start:end]

        start = raw.find(COLOR_CODE_END, end + len(COLOR_CODE_START))
        if start == -1:
            finished = True
        else:
            start += 1

    return clean

