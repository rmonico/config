#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import cpf as cpf_
import rg as rg_


class TestCPFModule(unittest.TestCase):
    def testCPFCalculoDigitos(self):
        digito_verificador = cpf_.digitos(cpf_.tolist('323490008'))

        self.assertEquals(digito_verificador, [6, 9])

    def testCPFGerar(self):
        cpf = cpf_.gerar()

        self.assertRegex(cpf_.tostr(cpf), '^[0-9]{11}$')

    def testCPFPontuar(self):
        cpf = cpf_.gerar()

        cpf = cpf_.pontuar(cpf_.tostr(cpf))

        self.assertRegex(cpf, '^([0-9]{3}\.){2}[0-9]{3}-[0-9]{2}$')

    def testCPFValido(self):
        self.assertTrue(cpf_.valido(cpf_.tolist('32349000869')))


class TestRGModule(unittest.TestCase):
    def testRGCalculoDigito(self):
        digito_verificador = rg_.digito(rg_.tolist('41848878'))

        self.assertEquals(digito_verificador, 2)

    def testRGDigitoToList(self):
        self.assertEquals(rg_.tolist('123456782'), [1, 2, 3, 4, 5, 6, 7, 8, 2])

    def testRGDigito0ToList(self):
        self.assertEquals(rg_.tolist('123456710'),
                          [1, 2, 3, 4, 5, 6, 7, 1, 10])

    def testRGDigitoXToList(self):
        self.assertEquals(rg_.tolist('12345677X'),
                          [1, 2, 3, 4, 5, 6, 7, 7, 11])

    def testRGDigitoToStr(self):
        rg = [1, 2, 3, 4, 5, 6, 7, 8, 2]
        self.assertEquals(rg_.tostr(rg), '123456782')

    def testRGDigito0ToStr(self):
        rg = [1, 2, 3, 4, 5, 6, 7, 7, 10]
        self.assertEquals(rg_.tostr(rg), '123456770')

    def testRGDigitoXToStr(self):
        rg = [1, 2, 3, 4, 5, 6, 7, 7, 11]
        self.assertEquals(rg_.tostr(rg), '12345677X')

    def testRGGerar(self):
        rg = rg_.gerar()

        self.assertRegex(rg_.tostr(rg), '^[0-9]{8}([0-9,X])$')

    def testRGPontuar(self):
        rg = rg_.gerar()

        rg = rg_.pontuar(rg_.tostr(rg))

        self.assertRegex(rg, '^[0-9]{2}(\.[0-9]{3}){2}-[0-9]$')

    def testRGValido(self):
        self.assertTrue(rg_.valido(rg_.tolist('418488782')))
