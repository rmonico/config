def tolist(documento: str, conversor: callable = int) -> list:
    return [conversor(algarismo) for algarismo in documento]


def tostr(documento: list, conversor: callable = str) -> str:
    l = [conversor(algarismo) for algarismo in documento]

    return ''.join(l)
