#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logger_wrapper
import sys


def parse_command_line():
    '''
    Reference: https://docs.python.org/3/library/argparse.html
    '''
    parser = argparse.ArgumentParser(description='Initialize reminder')

    parser.add_argument('config_file',
                        type=argparse.FileType('r'),
                        help='Configuration file to read from')

    logger_wrapper.make_verbosity_argument(parser)

    return parser.parse_args()


def error(message, exit_code=1):
    global logger
    logger.critical(message)
    sys.exit(exit_code)


def load_configs(args):
    import yaml

    return yaml.safe_load(args.config_file)


def load_reminders(configs):
    import reminder

    for c in configs:
        c['foreground'] = False
        c.setdefault('title', 'Reminder')
        args = c.pop('args')

        if (returncode := reminder.main(*args, **c)) != 0:
            return returncode

    return 0


def main():
    global args

    args = parse_command_line()

    logger_wrapper.configure(args.verbosity)
    '''
    Logger reference: https://docs.python.org/3/library/logging.html
    '''
    global logger
    logger = logger_wrapper.get(__name__)

    configs = load_configs(args)

    return load_reminders(configs)


if __name__ == '__main__':
    returncode = main() or 0

    logger.warning(f'Exiting with status {returncode}')

    sys.exit(returncode)
