#!/usr/bin/env perl

use AnyEvent::I3;
my $prefix = quotemeta(shift);
my @n = sort map { /^${prefix}_(\d+)$/ && $1 } @{i3->get_marks->recv};
my $next = ($n[-1] || 0) + 1;
i3->command("mark ${prefix}_${next}")->recv

