import unittest
# Create a link on ~/bin/api -> ~/bin/api.py
import api
from argparse import Namespace


class Logger(object):
    def warning(self, message):
        pass


api.logger = Logger()


class APITests(unittest.TestCase):
    def test_mount_url_with_basepath_and_with_path(self):
        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='base',
                      ignore_base_path=False,
                      path='path'))

        self.assertEquals(url, 'protocol://host:8080/base/path')

    def test_mount_url_with_basepath_and_without_path(self):
        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='base',
                      ignore_base_path=False,
                      path=''))

        self.assertEquals(url, 'protocol://host:8080/base')

    def test_mount_url_without_basepath_and_with_path(self):
        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='',
                      ignore_base_path=False,
                      path='path'))

        self.assertEquals(url, 'protocol://host:8080/path')

    def test_mount_url_without_basepath_and_without_path(self):
        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='',
                      ignore_base_path=False,
                      path=''))

        self.assertEquals(url, 'protocol://host:8080/')

    @unittest.skip
    def test_mount_url_path_without_start_slash(self):
        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='/base_path',
                      ignore_base_path=False,
                      path='path'))

        self.assertEquals(url, 'protocol://host:8080/base_path/path')

    @unittest.skip
    def test_mount_url_path_with_end_slash(self):
        api.logger = Logger()

        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='/base_path',
                      ignore_base_path=False,
                      path='path/'))

        self.assertEquals(url, 'protocol://host:8080/base_path/path')

    @unittest.skip
    def test_mount_url_base_path_without(self):
        url = api.mount_url(
            Namespace(protocol='protocol',
                      host='host',
                      port=8080,
                      base_path='/base_path',
                      ignore_base_path=False,
                      path='/path'))

        self.assertEquals(url, 'protocol://host:8080/base_path/path')


if __name__ == '__main__':
    unittest.main()
