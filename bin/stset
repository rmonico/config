#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys


def list_diff(operation: str, a_list, b_list, conversor):
    """
    a-b:  Dadas duas listas ordenadas, devolve apenas os itens em A
    b-a:  Dadas duas listas ordenadas, devolve apenas os itens em B
    a^b:  Dadas duas listas ordenadas, devolve apenas os itens em A e B (intersecção)
    a+b:  Dadas duas listas ordenadas, devolve os itens em A ou B (união)
    """

    if operation == 'b-a':
        x = b_list
        b_list = a_list
        a_list = x
        operation = 'a-b'

    a_idx = 0
    b_idx = 0
    while True:
        a = conversor(a_list[a_idx]) if a_idx < len(a_list) else sys.maxsize
        b = conversor(b_list[b_idx]) if b_idx < len(b_list) else sys.maxsize

        if a < b:
            if operation == 'a-b' or operation == 'a+b':
                yield a
            a_idx += 1

        if a == b:
            if operation == 'a^b' or operation == 'a+b':
                yield a
            a_idx += 1
            b_idx += 1

        if a > b:
            if operation == 'a+b':
                yield b
            b_idx += 1

        if a_idx == len(a_list) and b_idx == len(b_list):
            return



def _list_diff_tests():
    def check(actual, expected, message):
        if actual != expected:
            raise Exception(f'{message}: expected: {str(expected)}, actual: {actual}')

    a_b = list(list_diff('a-b', [1, 2, 3, 4], [2, 3]))
    check(a_b, [1, 4], 'a-b test')

    b_a = list(list_diff('b-a', [2, 3], [1, 2, 3, 4]))
    check(b_a, [1, 4], 'b-a test')

    intersection = list(list_diff('a^b', [1, 2], [2, 3]))
    check(intersection, [2], 'a^b test')

    union = list(list_diff('a+b', [1, 2], [2, 3]))
    check(union, [1, 2, 3], 'a+b test')

    print('Tests OK')

# _list_diff_tests()


def operation(raw: str) -> str:
    if raw in ['a-b', 'b-a', 'a+b', 'a^b']:
        return raw

    match raw:
        case 'a_but_b':
            return 'a-b'
        case 'b_but_a':
            return 'b-a'
        case 'union':
            return 'a+b'
        case 'intersection':
            return 'a^b'
        case _:
            raise ArgumentError(f'Invalid operation: {raw}')


def parse_command_line():
    '''
    Reference: https://docs.python.org/3/library/argparse.html
    '''
    from argparse import ArgumentParser, FileType

    parser = ArgumentParser(description='Set operations between two files')

    parser.add_argument('--text',
                        '-t',
                        action='store_true',
                        help='Treat lines in files as text, instead numbers')

    parser.add_argument('a_file',
                        nargs='?',
                        type=FileType('r'),
                        default=sys.stdin,
                        help='Set "A" (file name)')

    parser.add_argument('operation',
                        choices=['a-b', 'a_but_b', 'b-a', 'b_but_a', 'a+b', 'union', 'a^b', 'intersection'],
                        type=operation,
                        help='Operation to perform')

    parser.add_argument('b_file',
                        nargs='?',
                        type=FileType('r'),
                        help='Set "B" (file name)')

    return parser.parse_args()


def main():
    global args

    args = parse_command_line()

    a_set = [ l.strip() for l in args.a_file.readlines() ]
    b_set = [ l.strip() for l in args.b_file.readlines() ]
    
    conversor = str if args.text else int

    for line in list_diff(args.operation, a_set, b_set, conversor):
        print(line)


if __name__ == '__main__':
    returncode = main() or 0

    exit(returncode)

