colorscheme cobalt2

let g:VIM_HOME="~/.config/nvim"
let g:INIT_VIM=g:VIM_HOME .. "/init.vim"

let g:TASKWARRIOR_INIT=g:VIM_HOME .. "taskwarrior.vim"

" All folds open by default
" autocmd BufWinEnter * silent! :%foldopen!
" autocmd BufRead * silent! :%foldopen!
autocmd BufEnter * silent! :%foldopen!

set scrolloff=3

set relativenumber number

set cursorline

set clipboard=unnamedplus

" Disable cursor blinking
set guicursor+=a:blinkon0


set wrap
set ignorecase

set nohlsearch
set incsearch

" Use space as tabs
set tabstop=4 shiftwidth=4 expandtab

" let g:vimspector_enable_mappings = 'HUMAN'
set foldmethod=syntax
" packadd vimspector

syntax on

function! Redirect(cmd)
  redir => message
  silent execute a:cmd
  redir END
  if empty(message)
    echoerr "no output"
  else
    " use "new" instead of "tabnew" below if you prefer split windows instead of tabs
    tabnew
    setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
    silent put=message
  endif
endfunction

command! -nargs=+ -complete=command Redirect call Redirect(<q-args>)

command RootWrite :execute ':silent w !sudo tee % > /dev/null' | :edit!

command! Datestamp :call feedkeys('i[' .. trim(system("date '+\%d/\%m/\%Y'")) .. ']  ')

command! Date :call feedkeys('a' .. trim(system("date '+\%d/\%m/\%Y'")) .. '  ')

command! Timestamp :call feedkeys('i[' .. trim(system("date '+\%Y/\%m/\%d \%H:\%M:\%S'")) .. ']  ')


au! BufRead *.table :execute 'set nowrap | TableModeEnable'
autocmd! FileType nerdtree :set number
autocmd! FileType nerdtree :set relativenumber

" Sources at: https://github.com/mboughaba/i3config.vim
augroup i3config_ft_detection
  au!
  autocmd BufNewFile,BufRead ~/config/i3/config* set filetype=i3config
augroup end


imap <A-d> <ESC>ldwi

nmap <C-l> :bn<CR>
nmap <C-h> :bp<CR>
nmap <A-l> :ls<CR>:b
nmap <C-d> :bn<CR>:bd #<CR>
nmap <C-T> :NERDTreeToggle<CR>

" Abre este arquivo
nmap z0 :execute 'edit' .. g:INIT_VIM<CR>

" Set python breakpoint
" FIXME Está sobrescrevendo o atalho do nvim
" FIXME Fazer funcionar apenas para arquivos .py
nmap zb Obreakpoint()<ESC>j_:w<CR>

" Mover linha abaixo/acima
nmap J "zddj"zP
nmap K "zddk"zP

" Mover task baixo
nmap zj Vjxjp
" Mover task acima
nmap zk Vjxkkkp

" Nova task
" Tem um espaço no fim da linha abaixo
nmap zn o<CR>- 

" Nova task ao final do arquivo
" Tem um espaço no fim da linha abaixo
nmap zN o<CR>- 

" Excluir task
nmap zd ?^- <CR>V/^- <CR>kd


" Tabs
nmap th :tabprevious<CR>
nmap tH :tabmove -1<CR>
nmap tl :tabnext<CR>
nmap tL :tabmove<CR>

nmap tn :tabnew<CR>
nmap tc :tabclose<CR>


" Teclas gerais
imap <C-s> <ESC>:w<CR>a
nmap <C-n> <ESC>:enew<CR>
nmap <C-q> <cmd>qa!<CR>


" Telescope
nmap B :Telescope buffers<CR>
nmap F :Telescope find_files<CR>


" nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
" nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>fs <cmd>Telescope lsp_document_symbols<cr>
nnoremap <leader>fr <cmd>Telescope lsp_references<cr>

lua require('telescope').setup({ pickers = { colorscheme = { enable_preview = true } } })


" Airline
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.dirty='⚡'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" Mason
lua require("mason").setup()

autocmd! FileType python luafile ~/./config/nvim/init_python_lsp_server.lua

