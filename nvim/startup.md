# Arquivos de inicialização do vim e variantes

## Pela ordem

| Variante | Arquivo 1 | Arquivo 2 | Arquivo 3  |
|----------|-----------|-----------|------------|
| gnvim    | gvim/init | gvim/main | config.vim |
| nvim     | gvim/init | gvim/main | config.vim |
| gvim     | vim/vimrc | vim/main  | config.vim |
| vim      | vim/vimrc | vim/main  | config.vim |

## Por arquivo
 
|       | gvim/init | gvim/main | vim/vimrc | vim/main | config.vim |
|-------|:---------:|:---------:|:---------:|:--------:|:----------:|
| gnvim |     x     |     x     |           |          |      x     |
| nvim  |     x     |     x     |           |          |      x     |
| gvim  |           |           |     x     |     x    |      x     |
| vim   |           |           |     x     |     x    |      x     |


**Os arquivos ginit.vim não foram executados por nenhum dos editores**


Código usado para gerar esta informação:

```vim
" Arquivo executado por:
" gnvim: 
" nvim: 
" gvim:
" vim: 

let <vim or gvim>_<script name>="yes"
let script='<vim or gvim>/<script name>'
if exists('g:configfiles')
  let configfiles .= script
else
  let configfiles = script
endif
```

Atualizado em **24/nov/2021**

