" http://www.vanhemert.co.uk/vim/vimacros/ruler2.vim
" https://stackoverflow.com/questions/4548442/is-there-a-way-on-vim-to-show-all-column-numbers-in-the-current-buffer-line/4548918#4548918
" TODO Show current column in bold
" TODO Show word breaks in colors -> Não descobri como fazer isso, o jeito vai
" ser fazer um loop enviando 'w' e salvando a posição do cursor até mudar de
" linha ou chegar no final do arquivo
function! RulerStr()
:python3 << endPython
# This value may vary, I set to my configuration
nonEditorLeftArea = 4

fixedStringPrefix = ' '

def rulerPrefix():
    return fixedStringPrefix + ' ' * nonEditorLeftArea

terminalCols = int(vim.eval("&columns"))

maxCols = terminalCols - nonEditorLeftArea

baseStep = '....+....'
baseRuler = [baseStep]

for i in range(0, maxCols // 10):
    indicator = str(i+1)

    baseRuler.append(indicator + baseStep[len(indicator)-1:])

reverseRuler = baseRuler[1:] + [baseStep]

reverseRuler.reverse()

# Dont need to calculate the next three every time
# I got these two 8 by tests,  I dont know why
rulerStr = ''.join(baseRuler)[:-8]
reverseRulerStr = ''.join(reverseRuler)[8:]
ruleCenter = len(reverseRulerStr)
curCol = vim.windows[0].cursor[1]

finalRuler = rulerPrefix()
if curCol > 0:
    finalRuler += reverseRulerStr[-curCol:]
finalRuler += str(curCol+1) + rulerStr[len(str(curCol+1))-1:terminalCols - curCol - nonEditorLeftArea - 1]

vim.command(f'let ruler="{finalRuler}"')

endPython

    return ruler

endfunction

let s:saved_stl = {}

"...+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....8....+....9....+....10...+....11...+....12...+....13...+....14...+....15...+....16...+....17...+....18...+....19.
function! ToggleRuler()
  let buf = bufnr('%')
  if has_key(s:saved_stl, buf)
    let &l:stl = s:saved_stl[buf]
    unlet s:saved_stl[buf]
    setlocal cursorcolumn
  else
    let s:saved_stl[buf] = &l:stl
    setlocal stl=%{RulerStr()}
    setlocal nocursorcolumn
  endif
endfunction

nmap J :call ToggleRuler()<CR>:set cursorcolumn!<CR>

