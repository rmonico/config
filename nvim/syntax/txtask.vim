" Vim syntax file
" Language: TXTask TODO file
" Maintainer: Rafael Monico
" Latest Revision: 19 July 2021

" if exists("b:txtask")
"   finish
" endif

syntax match header '^:: .\+$'

syntax match comment '^\( \{2}\)*; .*$'
" Inicial
" syntax match comment '^; .*$'
syntax region blockcomment start='^\( \{2}\)*;;$' end='^\( \{2}\)*;;$' fold

syntax match task '^[-x] .*$' contains=tag_
syntax match subtask '^\( \{2}\)\+[-x] .*$' contains=tag_
syntax match tag_ '#[a-z_][a-z0-9_\.-]*\(:[a-zA-Z0-9\/_\-\.\,]\+\)\?'

" TODO: reuse above line
syntax match context_tag_ '##[a-z_][a-z0-9_\.-]*\(:[a-zA-Z0-9\/_\-\.\,]\+\)\?'

" Arrumar o end, tem que finalizar quando encontrar uma task/subtask de menor
" nível 
" syntax region subtaskFold start='^\( \{2}\)*[-x] .*$' end="^$" fold

highlight! def link header Title

highlight! def link comment Comment
highlight! def link blockcomment Comment

highlight! def link task Statement
highlight! def link subtask Constant
highlight! def link tag_ Identifier
highlight! def link context_tag_ Title

